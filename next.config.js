module.exports = () => {
  return {
    reactStrictMode: false,
    trailingSlash: true,
    images: {
      formats: ['image/webp'],
      minimumCacheTTL: 60,
    },
    env: {
      SITE_LINK: process.env.NEXT_SITE_LINK
    },
  }
}
