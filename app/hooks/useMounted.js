import React from 'react';

export function useMounted(returnState, callback) {
  const [mounted, setMounted] = React.useState(false);

  React.useEffect(() => {
    setMounted(true);
    callback ? callback() : null;
  }, []);

  return returnState && mounted;
}
