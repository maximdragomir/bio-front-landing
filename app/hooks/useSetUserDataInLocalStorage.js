import React from 'react';

export function useSetUserDataInLocalStorage() {
  React.useEffect(() => {
    fetch('https://www.iplocate.io/api/lookup/')
      .then(res => res.json())
      .then(response => {
        localStorage.setItem('bioCountryCode', response?.country_code)
        localStorage.setItem('bioUserData', JSON.stringify(response))
      })
      .catch((err) => {
        console.log('Request failed:', err);
      });
  }, []);
}
