import React from "react";

export function useDebounce(callback, delay) {
  const timer = React.useRef(null);

  return React.useCallback(
    (...args) => {
      timer.current && clearTimeout(timer.current);

      timer.current = setTimeout(() => {
        callback(...args);
      }, delay);
    },
    [callback, delay],
  );
}