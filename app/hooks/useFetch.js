import React from "react";

export function useFetch(request) {
  const defaultData = {
    data: null,
    loading: false,
    error: false,
  };
  const [fetchData, setFetchData] = React.useState({
    ...defaultData,
  });

  const fetching = async (...args) => {
    setFetchData({
      ...defaultData,
      loading: true,
    });

    await request(...args)
      .then(({ normalizedData }) => {
        setFetchData({
          ...defaultData,
          data: normalizedData,
        });
      })
      .catch((error) => {
        setFetchData({
          ...defaultData,
          error: error.message,
        });
      });
  };

  return [fetching, fetchData];
}
