import React from "react";

export function useWindowSize() {
  const [windowSize, setWindowSize] = React.useState({
    width: 0,
    height: 0,
  });

  React.useEffect(() => {
    function handleResize() {
      setWindowSize((state) => ({
        ...state,
        width: window.innerWidth,
        height: window.innerHeight,
      }));
    }

    if(/mobi/i.test(navigator.userAgent)) {
      window.addEventListener('orientationchange', handleResize);
    } else {
      window.addEventListener('resize', handleResize);
    }
    handleResize();
    return () => {
      if(/mobi/i.test(navigator.userAgent)) {
        window.addEventListener('orientationchange', handleResize);
      } else {
        window.removeEventListener('resize', handleResize)
      }
    };
  }, []);

  return windowSize;
}