import React from 'react'
import AOS from 'aos'
import {AOS_CONFIG, LOADER_CONFIG} from "../utils/config";
import {useDebounce} from "./useDebounce";

export function useAos(settings) {
  // debounce need for the  correctly work aos
	const debounceAos = useDebounce(() => {
		AOS?.init({
			duration: AOS_CONFIG.DURATION,
			once: AOS_CONFIG.ONCE,
			easing: AOS_CONFIG.EASING,
			// offset: 200,
			...settings,
		})
	}, LOADER_CONFIG.DELAY / 2);

	React.useEffect(() => {
		debounceAos();

		window.addEventListener('load', AOS?.refresh)
	}, [])
}
