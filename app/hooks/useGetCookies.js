import React from 'react'
import Cookies from "universal-cookie";

export function useGetCookies(cookieName) {
  const cookies = new Cookies();

  const cookieState = React.useRef(null);
  cookieState.current = cookies.get(cookieName) ? cookies.get(cookieName) : null;

  return JSON.parse(JSON.stringify(cookieState.current))
}
