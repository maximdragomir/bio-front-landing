import React from "react";

export function useClickOutside(ref, handler) {

	function handleClickOutside(event) {
		if (!ref?.current || ref?.current.contains(event.target)) {
			return;
		}

		handler(event);
	}

	React.useEffect(() => {
		window.addEventListener('mousedown', handleClickOutside);
		window.addEventListener('touchstart', handleClickOutside);

		return () => {
			window.removeEventListener('mousedown', handleClickOutside);
			window.removeEventListener('touchstart', handleClickOutside);
		};
	}, [ref, handler]);
}
