export const plansData = [
  {
    name: 'basic',
    sum: 'free',
    type: 'forever',
    list: [
      {
        active: true,
        text: 'Only one site',
      },
      {
        active: true,
        text: 'Custom profile image, title and bio description',
      },
      {
        active: true,
        text: 'Pre-defined design themes',
      },
      {
        active: true,
        text: 'Buttons, links and font styles',
      },
      {
        active: true,
        text: 'Button thumbnails',
      },
      {
        active: true,
        text: 'Custom backgrounds',
      },
      {
        active: true,
        text: 'Total page views',
      },
      {
        active: true,
        text: 'Total links and buttons clicks, CTR',
      },
      {
        active: true,
        text: 'Regular 8 hours',
      },
      {
        active: false,
        text: 'Individual link/button analytics',
      },
      {
        active: false,
        text: 'Clicks and views geography',
      },
      {
        active: false,
        text: 'Data filter by period',
      },
      {
        active: false,
        text: 'Data export',
      },
      {
        active: false,
        text: 'Remove Lift Watermark',
      },
    ],
    action: {
      text: 'Start with FREE',
      modifier: 'secondary',
      eventName: 'premium free pressed',
      context: 'landing (переход в pricing с лендинга)'
    }
  },
  {
    name: 'Pro',
    sum: '$9.99',
    type: 'per month',
    list: [
      {
        active: true,
        text: 'Unlimited sites',
      },
      {
        active: true,
        text: 'Custom profile image, title and bio description',
      },
      {
        active: true,
        text: 'Pre-defined design themes',
      },
      {
        active: true,
        text: 'Buttons, links and font styles',
      },
      {
        active: true,
        text: 'Button thumbnails',
      },
      {
        active: true,
        text: 'Custom backgrounds',
      },
      {
        active: true,
        text: 'Total page views',
      },
      {
        active: true,
        text: 'Total links and buttons clicks, CTR',
      },
      {
        active: true,
        text: '24/7 Priority customer care',
      },
      {
        active: true,
        text: 'Individual link/button analytics',
      },
      {
        active: true,
        text: 'Clicks and views geography',
      },
      {
        active: true,
        text: 'Data filter by period',
      },
      {
        active: true,
        text: 'Data export',
      },
      {
        active: true,
        text: 'Remove Lift Watermark',
      },
    ],
    action: {
      text: 'Join the PROs',
      modifier: false,
      eventName: 'premium pro pressed',
      context: 'landing (переход в pricing с лендинга)'
    }
  },
]

// export const plansData2 = {
//
// }
