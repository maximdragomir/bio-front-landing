// Data for index page

// hero section
const sectionMainRoot = '/images/section-main'
export const heroBioSitesData = [
  {
    title: 'Music festival',
    subtitle: 'Celebrate diversity',
    background: `${sectionMainRoot}/slide1/bg-big.jpeg`,
    logo: `${sectionMainRoot}/slide1/logo.jpeg`,
    socials: [
      {
        name: 'spotify',
        link: ''
      },
      {
        name: 'soundcloud',
        link: ''
      },
      {
        name: 'instagram',
        link: ''
      },
      {
        name: 'facebook',
        link: ''
      },
      {
        name: 'vimeo',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Latest headlines',
        image: `${sectionMainRoot}/slide1/button1.jpeg`,
      },
      {
        name: 'Last year',
        image: `${sectionMainRoot}/slide1/button2.jpeg`,
      },
      {
        name: 'Let’s camp',
        image: `${sectionMainRoot}/slide1/button3.jpeg`,
      },
      {
        name: 'Photo gallery',
        image: `${sectionMainRoot}/slide1/button4.jpeg`,
      },
    ],
  },
  {
    title: 'Jessica',
    subtitle: 'Model, Influencer',
    background: ``,
    logo: `${sectionMainRoot}/slide2/logo.jpeg`,
    socials: [
      {
        name: 'instagram',
        link: ''
      },
      {
        name: 'facebook',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'TikTok',
        image: `${sectionMainRoot}/slide2/button1.jpeg`,
      },
      {
        name: 'Instagram',
        image: `${sectionMainRoot}/slide2/button2.jpeg`,
      },
      {
        name: 'Youtube',
        image: `${sectionMainRoot}/slide2/button3.jpeg`,
      },
      {
        name: 'Pinterest',
        image: `${sectionMainRoot}/slide2/button4.jpeg`,
      },
    ],
  },
  {
    title: 'Olivia',
    subtitle: 'Knitting tips ',
    background: `${sectionMainRoot}/slide3/bg-big.jpeg`,
    logo: `${sectionMainRoot}/slide3/logo.jpeg`,
    socials: [
      {
        name: 'mail',
        link: ''
      },
      {
        name: 'instagram',
        link: ''
      },
      {
        name: 'facebook',
        link: ''
      },
      {
        name: 'tiktok',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'My Shop',
        image: `${sectionMainRoot}/slide3/button1.jpeg`,
      },
      {
        name: 'YouTube Channel',
        image: '',
      },
      {
        name: 'Shop Supplies',
        image: '',
      },
      {
        name: 'Personal Website',
        image: '',
      },
    ],
  },
  {
    title: 'SeaSide',
    subtitle: 'Resin Artist',
    background: `${sectionMainRoot}/slide4/bg-big.jpeg`,
    logo: `${sectionMainRoot}/slide4/logo.jpeg`,
    socials: [
      {
        name: 'pinterest',
        link: ''
      },
      {
        name: 'instagram',
        link: ''
      },
      {
        name: 'facebook',
        link: ''
      },
      {
        name: 'patreon',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'BUY ON ETSY',
        image: ''
      },
      {
        name: 'MY WEBSITE',
        image: `${sectionMainRoot}/slide4/button1.jpeg`
      },
      {
        name: 'TUTORIALS & INSPIRATION',
        image: ''
      },
      {
        name: 'BUY SUPPLIES',
        image: ''
      },
    ],
  },
  {
    title: 'Lucas',
    subtitle: 'Yoga enthusiast',
    background: `${sectionMainRoot}/slide5/bg-big.jpeg`,
    logo: `${sectionMainRoot}/slide5/logo.jpeg`,
    socials: [
      {
        name: 'spotify',
        link: ''
      },
      {
        name: 'tiktok',
        link: ''
      },
      {
        name: 'vimeo',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Yoga for begginers',
      },
      {
        name: 'Nutrition',
      },
      {
        name: 'World yoga camp',
      },
      {
        name: 'The 10 most yoga mistakes',
      },
    ],
  },
  {
    title: 'Script',
    subtitle: 'Creative agency',
    background: '',
    logo: `${sectionMainRoot}/slide6/logo.jpeg`,
    socials: [
      {
        name: 'facebook',
        link: ''
      },
      {
        name: 'behance',
        link: ''
      },
      {
        name: 'dribbble',
        link: ''
      },
      {
        name: 'pinterest',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Who we are',
        image: `${sectionMainRoot}/slide6/button1.jpeg`
      },
      {
        name: 'What we do',
      },
      {
        name: 'Our cases',
      },
      {
        name: 'Hire us!',
      },
    ],
  }
]
export const heroCardsData = [
  {
    title: 'Gather like-minded people',
    image: `${sectionMainRoot}/slide1/bg-small.jpeg`,
  },
  {
    title: 'Introduce yourself',
    image: `${sectionMainRoot}/slide2/bg-small.jpeg`,
  },
  {
    title: 'Sell products',
    image: `${sectionMainRoot}/slide3/bg-small.jpeg`,
  },
  {
    title: 'Present your work',
    image: `${sectionMainRoot}/slide4/bg-small.jpeg`,
  },
  {
    title: 'Share experience',
    image: `${sectionMainRoot}/slide5/bg-small.jpeg`,
  },
  {
    title: 'Find new customers',
    image: `${sectionMainRoot}/slide6/bg-small.jpeg`,
  },
]

// social section
const sectionSocialRoot = '/images/section-social'
export const socialCardsData = [
  {
    modifier: 'l',
    title: 'No designers needed',
    image: `${sectionSocialRoot}/bg1.jpeg`,
  },
  {
    modifier: 'l',
    title: 'No developers needed',
    image: `${sectionSocialRoot}/bg2.jpeg`,
  },
  {
    modifier: 'l',
    title: 'No specific skills',
    image: `${sectionSocialRoot}/bg3.jpeg`,
  }
]

// example section
const sectionExampleRoot = '/images/section-example'
export const exampleBioSitesData = [
  {
    title: 'Personal blog',
    subtitle: 'Welcome to my life',
    background: `${sectionExampleRoot}/bg.jpeg`,
    logo: `${sectionExampleRoot}/logo.jpeg`,
    socials: [
      {
        name: 'twitter',
        link: ''
      },
      {
        name: 'soundcloud',
        link: ''
      },
      {
        name: 'spotify',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Traveling',
        image: '',
      },
      {
        name: 'My Look book',
        image: `${sectionExampleRoot}/button2.jpeg`,
      },
      {
        name: 'Inspirational music',
        image: '',
      },
      {
        name: 'Follow my instagram',
        image: '',
      },
    ],
  },
  {
    title: 'Personal blog',
    subtitle: 'Welcome to my life',
    background: `${sectionExampleRoot}/bg.jpeg`,
    logo: `${sectionExampleRoot}/logo.jpeg`,
    socials: [
      {
        name: 'twitter',
        link: ''
      },
      {
        name: 'soundcloud',
        link: ''
      },
      {
        name: 'spotify',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Summer Greece',
        image: `${sectionExampleRoot}/button1.jpeg`,
      },
      {
        name: 'My Look book',
        image: `${sectionExampleRoot}/button2.jpeg`,
      },
      {
        name: 'Inspirational music',
        image: '',
      },
      {
        name: 'Follow my instagram',
        image: '',
      },
    ],
  },
  {
    title: 'Personal blog',
    subtitle: 'Welcome to my life',
    background: `${sectionExampleRoot}/bg.jpeg`,
    logo: `${sectionExampleRoot}/logo.jpeg`,
    socials: [
      {
        name: 'twitter',
        link: ''
      },
      {
        name: 'soundcloud',
        link: ''
      },
      {
        name: 'spotify',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Summer Greece',
        image: `${sectionExampleRoot}/button1.jpeg`,
      },
      {
        name: 'My Look book',
        image: `${sectionExampleRoot}/button2.jpeg`,
      },
      {
        name: 'Inspirational music',
        image: '',
      },
      {
        name: 'Follow my instagram',
        image: '',
      },
    ],
  },
]

// benefits section
const sectionBenefitsRoot = '/images/section-benefits';
export const benefitsCardsData = [
  {
    id: 'themes',
    title: 'themes',
    aos: 'fade-left',
    items: [
      `${sectionBenefitsRoot}/theme1.jpeg`,
      `${sectionBenefitsRoot}/theme2.jpeg`,
      `${sectionBenefitsRoot}/theme3.jpeg`,
    ],
  },
  {
    id: 'color',
    title: 'color',
    aos: 'fade-up',
    items: [
      'conic-gradient(from 180deg at 50% 50%, #FF7700 0deg, #F4F400 58.12deg, #0CFF0A 116.25deg, #00FBFE 180deg, #002CFF 243.75deg, #FF00E4 301.88deg, #FF0030 360deg)',
      '#C3281C',
      '#DF6868',
      '#DF7B45',
      '#E8CD6D',
      '#8EECB3',
      '#55BCA6',
      '#90FBDC',
      '#3B91E3',
      '#67C2F9',
      '#CF54F3',
      '#B52D62',
      '#EB5281',
      '#B9ABA5',
      '#616161',
    ],
  },
  {
    id: 'font',
    title: 'font',
    aos: 'fade-down-right',
    items: [
      `${sectionBenefitsRoot}/font1.png`,
      `${sectionBenefitsRoot}/font2.png`,
      `${sectionBenefitsRoot}/font3.png`,
      `${sectionBenefitsRoot}/font4.png`,
    ]
  },
]
export const benefitsCardsData2 = [
  {
    id: 'ceo',
    title: 'ceography',
    aos: 'fade-down-left',
    items: [
      {
        image: `${sectionBenefitsRoot}/country/usa.svg`,
        name: 'usa',
        numbers: '21 167',
      },
      {
        image: `${sectionBenefitsRoot}/country/uk.svg`,
        name: 'uk',
        numbers: '14 940',
      },
      {
        image: `${sectionBenefitsRoot}/country/germany.svg`,
        name: 'germany',
        numbers: '7 208',
      },
      {
        image: `${sectionBenefitsRoot}/country/spain.svg`,
        name: 'spain',
        numbers: '5 810',
      },
    ]
  },
  {
    id: 'clicks',
    title: 'most clicked',
    aos: 'fade-up',
    items: [
      {
        name: 'instagram',
        numbers: '10 045',
      },
      {
        name: 'telegram',
        numbers: '10 012',
      },
      {
        name: 'twitter',
        numbers: '8 291',
      },
      {
        name: 'pinterest',
        numbers: '7 415',
      },
    ]
  },
  {
    id: 'visits',
    title: 'page visits',
    visits: '371 836',
    stats: '+32%',
    aos: 'fade-down-right',
  }
]

// whom section
const sectionWhomRoot = '/images/section-whom';
export const whomCardsData = [
  {
    url: 'elevenband',
    logo: '',
    title: 'Eleven',
    subtitle: 'Musician',
  },
  {
    url: 'nooneclothesbrand',
    logo: '',
    title: 'Noone Clothes',
    subtitle: 'E-Commerce',
  },
  {
    url: 'jeremypage',
    logo: '',
    title: 'Jeremy',
    subtitle: 'Blogger',
  },
  {
    url: 'duckstationfood',
    logo: '',
    title: 'Duck Station',
    subtitle: 'Restaurant',
  },
  {
    url: 'esportsplayer',
    logo: '',
    title: 'AliciaKBoom',
    subtitle: 'Esports',
  },
  {
    url: 'designbureau',
    logo: '',
    title: '23floor',
    subtitle: 'Studio',
  },
  {
    url: 'realrieltor',
    logo: '',
    title: 'Rob Williams',
    subtitle: 'Rieltor',
  },
  {
    url: 'candybar',
    logo: '',
    title: 'Marzipan',
    subtitle: 'Candy bar',
  },
  {
    url: 'fineboat',
    logo: '',
    title: 'Fine Boat',
    subtitle: 'Handcrafted boats',
  },
  {
    url: 'banana',
    logo: '',
    title: 'Banana',
    subtitle: 'Influencer',
  },
  {
    url: 'easytravel',
    logo: '',
    title: 'Easy Travel',
    subtitle: 'Agency',
  },
  {
    url: 'vihair',
    logo: '',
    title: 'Vi Priessnitz',
    subtitle: 'Hairdresser',
  },
]
