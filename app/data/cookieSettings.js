export const cookieSettings = {
  essential: {
    title: 'Essential Cookies',
    description: 'These cookies enable core functionality such as security, verification of identity and network management. These cookies can’t be disabled.',
    showDescription: false,
    checked: true,
  },
  functional: {
    title: 'Functional Cookies',
    description: 'These cookies collect data to remember choices users make to improve and give a more personalised experience.',
    showDescription: false,
    checked: false,
  },
  marketing: {
    title: 'Marketing Cookies',
    description: 'These cookies are used to track advertising effectiveness to provide a more relevant service and deliver better ads to suit your interests.',
    showDescription: false,
    checked: false,
  },
  analytics: {
    title: 'Analytics Cookies',
    description: 'These cookies help us to understand how visitors interact with our website, discover errors and provide a better overall analytics.',
    showDescription: false,
    checked: false,
  },
}
