// Data for banner

const bannerRoot = '/images/banner';
export const bannerBioSitesData = [
  {
    title: 'Oscar Grahem',
    subtitle: 'Passionate photographer',
    background: `${bannerRoot}/site1/bg.jpeg`,
    logo: `${bannerRoot}/site1/logo.jpeg`,
    socials: [
      {
        name: 'instagram',
        link: ''
      },
      {
        name: 'facebook',
        link: ''
      },
      {
        name: 'vimeo',
        link: ''
      },
    ],
    buttons: [
      {
        name: 'Fashion',
        image: `${bannerRoot}/site1/button1.jpeg`,
      },
      {
        name: 'Portraites',
        image: `${bannerRoot}/site1/button2.jpeg`,
      },
      {
        name: 'Landscapes',
        image: `${bannerRoot}/site1/button3.jpeg`,
      },
      {
        name: 'Photo gallery',
        image: `${bannerRoot}/site1/button4.jpeg`,
      },
    ],
  },
  {
    title: 'Valerie Elashy',
    subtitle: 'Digital artist',
    background: `${bannerRoot}/site2/bg.jpeg`,
    logo: `${bannerRoot}/site2/logo.jpeg`,
    socials: [],
    buttons: [
      {
        name: 'Portfolio',
        image: '',
      },
      {
        name: 'My way',
        image: '',
      },
      {
        name: 'Hire me',
        image: '',
      },
    ],
  },
]
