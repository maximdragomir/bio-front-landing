export const capitalizeFirstLetter = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};
export const normalizedArray = (child) => {
	if (!child) {
		return [];
	}

	if (typeof child === 'object' && !child.hasOwnProperty('length')) {
		return [child];
	}

	return child;
};
