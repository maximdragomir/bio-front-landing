// APP CONSTANTS

export const SITE_NAME = 'lift.bio';
export const FB_PIXEL_ID = 482128656589519;
export const COOKIES_DOMAIN = '.lift.bio';
export const STAGE_API_DOMAIN = 'https://stage-api.lift.bio';
export const AMPLITUDE_KEY = 'e803faa7c80622c076163ca8304e6ba1'
export const COOKIE_ACCESS_REGION = [
  'AT',
  'BE',
  'BG',
  'HR',
  'CY',
  'CZ',
  'DK',
  'EE',
  'FI',
  'FR',
  'DE',
  'GR',
  'EL',
  'HU',
  'IE',
  'IT',
  'LV',
  'LT',
  'LU',
  'MT',
  'NL',
  'PL',
  'PT',
  'RO',
  'SK',
  'SI',
  'ES',
  'SE',
  'GB',
  'IM',
  'UK',
  // 'UA'
]

// Api
export const API_DOMAIN = {
  FB: `${STAGE_API_DOMAIN}/addition-user-data/fb`,
};
