export const generateFbc =  (fbclid, generatedFbc) => {
  if(!fbclid){
    return null;
  }
  generatedFbc = 'fb.1.' + Date.now() + '.' + fbclid;
  return generatedFbc;
}
