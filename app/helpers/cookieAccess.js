import {COOKIE_ACCESS_REGION} from "../utils/constants";

export const cookieAccess = () => COOKIE_ACCESS_REGION.includes(localStorage?.getItem('bioCountryCode'));
