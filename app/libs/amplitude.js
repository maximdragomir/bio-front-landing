import amplitude from 'amplitude-js';
import {AMPLITUDE_KEY} from "../utils/constants";

export const initAmplitude = () => {
  if (process.browser) {
    amplitude.getInstance().init(AMPLITUDE_KEY);
  }
};

export const amplitudeUserId = (userId) => {
  amplitude.getInstance().setUserId(userId);
};

export const amplitudeUserProperty = (properties) => {
  amplitude.getInstance().setUserProperties(properties);
};

export const amplitudeEvent = (eventType, eventProperties) => {
  amplitude.getInstance().logEvent(eventType, eventProperties);
};
