import Cookies from "universal-cookie";
import * as fbq from "../libs/fpixel";
import {sendFBData} from "../api";
import {generateFbc, getCookie} from "../helpers";

export const linkGenerator = () => {
  const cookies = new Cookies();
  const params = new URLSearchParams(window.location.search);
  const getUserData = JSON.parse(localStorage?.getItem('bioUserData'));
  let generatedFbc, url;

  const utm_source = params.get('utm_source') || '';
  const utm_campaign = params.get('utm_campaign') || '';
  const adset_name = params.get('adset_name') || '';
  const ad_name = params.get('ad_name') || '';
  const fbclid = params.get('fbclid');
  const gclid = params.get('gclid') || '';
  const ttclid = params.get('ttclid') || '';
  const campaign_id = params.get('campaign_id') || '';
  const adset_id = params.get('adset_id') || '';
  const ad_id = params.get('ad_id') || '';

  const getUserSite = cookies.get('liftUserSite');
  const userSite = `user_site_url=${getUserSite || ''}`;

  const getFbc = () => {
    const fbc = getCookie('_fbc');
    if (fbc) {
      return fbc;
    } else if (generatedFbc) {
      return generatedFbc;
    }

    let params = new URLSearchParams(window.location.search);
    let fbclid = params.get('fbclid');
    return fbc ? fbc : generateFbc(fbclid, generatedFbc);
  }
  const getFbp = () => {
    const fbp = getCookie('_fbp')
    return fbp ? fbp : null;
  }

  fbq.event('SignUp');
  if(fbclid) {
    const fbc = `fbc=${getFbc()}`;
    url = `${process.env.SITE_LINK}/sign-up?${fbc}&${userSite}`;

    const {ip, city, postal_code: zipCode, country_code: countryCode} = getUserData;
    sendFBData({
      ip: ip,
      url: window.location.href,
      fbp: getFbp(),
      fbc: getFbc(),
      userAgent: navigator.userAgent,
      city: city,
      zipCode: zipCode,
      countryCode: countryCode,
      userAdData: {
        adId: ad_id,
        adName: ad_name,
        adsetId: adset_id,
        adsetName: adset_name,
        campaignId: campaign_id,
        utmCampaign: utm_campaign,
        utmSource: utm_source,
      },
    }).then(data => {
      console.log(data)
      window.location.href = url
    }).catch((error) => {
      console.error(error)
      window.location.href = url
    });

  } else {
    url = `${process.env.SITE_LINK}/sign-up?${userSite}`;
    window.location.href = url;
  }
}


// for another clids
// } else if (gclid) {
//   utm_source = 'google_web';
//   return `https://lift.onelink.me/KndT?pid=${utm_source}&c=${utm_campaign}&af_sub2=${gclid}`
// } else if (ttclid) {
//   return `https://lift.onelink.me/KndT?pid=${utm_source}&c=${utm_campaign}&af_adset=${adset_name}&af_ad=${ad_name}&af_sub3=${ttclid}`
