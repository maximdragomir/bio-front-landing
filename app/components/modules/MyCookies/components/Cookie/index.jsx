import React from 'react';

import Dropdown from "../../../../elements/Dropdown";
import ToggleButton from "../../../../elements/ToggleButton";
import {Icon} from "../../../../elements/Icon";
import {useClickOutside} from "../../../../../hooks";

import styles from './Cookie.module.scss'

const Cookie = ({name, data, updateCookiesData}) => {
  const {checked, showDescription, title, description } = data;

  const [active, setActive] = React.useState(showDescription);

  const refDropdown = React.useRef(null);
  useClickOutside(refDropdown, () => setActive(false));

  const updateToggleValue = (value) => {
    updateCookiesData({
      [name]: {
        ...data,
        checked: value,
      }
    })
  }

  return (
    <div
      className={`
        ${styles['dropdown']}
        ${name ? styles['dropdown_' + name] : ''}
      `}
      ref={refDropdown}
    >
      <Dropdown
        state={active}
        header={
          <div className={`${styles['dropdown__header']}`}>
            <button
              onClick={() => setActive((state) => !state)}
              className={`
                ${styles['dropdown__button']}
                ${active ? styles['dropdown__button_active'] : ''}
              `}
              type='button'
            >
              <Icon
                name={'arrow'}
                className={`
                  ${styles['dropdown__button-icon']}
                `}
              />
              <span className={`${styles['dropdown__title']}`}>{title}</span>
            </button>
            {name === 'essential' ? (
              <span className={`${styles['dropdown__title']} ${styles['dropdown__title_always']}`}>(Always active)</span>
            ) : (
              <ToggleButton updateToggleValue={updateToggleValue} checked={checked} className={styles['dropdown__toggle']} />
            )}
          </div>
        }
        content={
          <div className={`${styles['dropdown__content']}`}>
            <p className={`${styles['dropdown__text']}`}>{description}</p>
          </div>
        }
      />
    </div>
  );
};

export default Cookie;
