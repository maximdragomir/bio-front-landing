import React from 'react';
import {CSSTransition, SwitchTransition} from "react-transition-group";
import Cookies from 'universal-cookie';

import Cookie from "./components/Cookie";
import RouteLink from "../../elements/RouteLink";
import {Icon} from "../../elements/Icon";
import {useKeyPress, useDebounce, useMounted, useWindowSize} from "../../../hooks";
import {cookieSettings} from "../../../data/cookieSettings";
import {COOKIES_DOMAIN} from "../../../utils/constants";

import styles from './MyCookies.module.scss'

const MyCookies = ({cookies, setCookies, getCookies}) => {
  const cookiesInstance = new Cookies();
  const {getCookiesData, getCookiesSettings} = getCookies;
  const [cookiesData, setCookiesData] = React.useState(cookieSettings);
  // Change cookie view
  const toggleCookies = () => {
    if(getCookiesData) {
      setCookies((state)=>({
        ...state,
        show: false,
        settings: false,
      }))
    } else {
      setCookies((state)=>({
        ...state,
        settings: !state.settings,
      }))
    }
  }

  // Helpers
  const updateCookiesData = (updatedObj) => {
    // update data when user change toggles
    setCookiesData((state)=>{
      return {
        ...state,
        ...updatedObj,
      }
    })
  }
  const setDataInCookie = (data) => {
    const {essential, functional, marketing, analytics} = data;

    setCookiesData(data);
    debounceShowCookies();

    const cookiesSettings = JSON.stringify([
      essential.checked,
      functional.checked,
      marketing.checked,
      analytics.checked,
    ])
    cookiesInstance.set('cookiesData', '1', { domain: COOKIES_DOMAIN });
    cookiesInstance.set('cookiesSettings', cookiesSettings, { domain: COOKIES_DOMAIN });
  }

  // Actions
  const rejectAction = () => {
    setDataInCookie(cookieSettings);
  }
  const acceptAction = () => {
    const updatedCookiesData = Object.keys(cookiesData)?.map((item) => {
      return {
        [item]: {
          ...cookiesData[item],
          checked: true,
        }
      }
    });
    const correctCookiesData = Object.assign({}, ...updatedCookiesData);

    setDataInCookie(correctCookiesData);
  }
  const saveAction = () => {
    setDataInCookie(cookiesData);
  }

  // Hooks
  const componentMounted = useMounted(true);
  const debounceShowCookies = useDebounce(() => {
    setCookies((state)=>({
      ...state,
      show: false,
      settings: false,
    }))
  }, 50);
  const windowSize = useWindowSize();
  const escapePress = useKeyPress('Escape');
  const refNotification = React.useRef(null);

  React.useEffect(() => {
    if(cookies.settings) {
      document.querySelector('body').classList.add('cookies')
    } else {
      document.querySelector('body').classList.remove('cookies')
    }
  }, [cookies.settings])
  React.useEffect(() => {
    componentMounted
    && cookies.settings
    && setCookies((state)=>({
        ...state,
        settings: escapePress,
      }))
  }, [escapePress]);
  React.useEffect(() => {
    const notificationBounding = refNotification?.current?.getBoundingClientRect()?.height + 24;
    document.body.style.paddingBottom = `${notificationBounding}px`;
  }, [windowSize])
  React.useEffect(() => {
    const updatedCookiesData = Object.keys(cookiesData)?.map((item, index) => {
      const cookieCondition = getCookiesSettings ? getCookiesSettings[index] : false;

      return {
        [item]: {
          ...cookiesData[item],
          checked: index > 0 ? cookieCondition : true,
        }
      }
    });
    const correctCookiesData = Object.assign({}, ...updatedCookiesData);
    setCookiesData(correctCookiesData);

    return () => {
      document.body.style.paddingBottom = '0px';
    }
  }, [])


  return (
    <div className={`${styles['cookies']} ${cookies.settings ? styles['cookies_active'] : ''}`}>
      <div
        className={`${styles['cookies__overlay']}`}
        onClick={toggleCookies}
      />
      <SwitchTransition>
        <CSSTransition
          key={cookies.settings}
          addEndListener={(node, done) => node.addEventListener('transitionend', done, false)}
          classNames={getCookiesData ? 'transition-fade' : 'transition-fadeMixBottom'}
        >
          {cookies.settings ? (
            <div className={`${styles['cookies__wrapper']} ${styles['cookies__wrapper_settings']}`}>
              <div className={`${styles['settings']}`}>
                <button
                  type='button'
                  className={styles['settings__close']}
                  onClick={toggleCookies}
                >
                  <Icon name={'close'} className={styles['settings__close-icon']} />
                </button>
                <div className={`${styles['settings__title']}`}>
                    Manage cookie settings
                </div>
                <div className={`${styles['settings__body']}`}>
                  {Object.keys(cookiesData)?.map((item) => {
                    return <Cookie key={item} name={item} data={cookiesData[item]} updateCookiesData={updateCookiesData} />
                  })}
                </div>
                <div className={`${styles['settings__actions']}`}>
                  <button
                    className={`
                      ${styles['c-button']}
                      ${styles['c-button_secondary']}
                      ${styles['settings__button']}
                      ${styles['settings__button_secondary']}
                    `}
                    type='button'
                    onClick={saveAction}
                  >
                    Save Settings
                  </button>
                  <button
                    className={`
                    ${styles['c-button']}
                    ${styles['c-button_secondary']}
                    ${styles['settings__button']}
                    ${styles['settings__button_secondary']}
                  `}
                    type='button'
                    onClick={rejectAction}
                  >
                    Reject All
                  </button>
                  <button
                    className={`
                    ${styles['c-button']}
                    ${styles['settings__button']}
                  `}
                    type='button'
                    onClick={acceptAction}
                  >
                    Accept All
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <div className={`${styles['cookies__wrapper']} ${styles['cookies__wrapper_notification']}`}>
              <div ref={refNotification} className={`${styles['notification']}`}>
                <p className={styles['notification__info']}>
                  <span className={styles['notification__text']}>We use cookies and similar technologies to enable services and functionality on our site and to understand your interaction with our service. By clicking on accept, you agree to our use of such technologies for marketing and analytics.</span>{' '}
                  <RouteLink link={'/cookie'} classes={`${styles['notification__text']} ${styles['notification__text_link']}`}>Cookie Policy</RouteLink>
                </p>
                <div className={styles['notification__action']}>
                  <button
                    className={`
                      ${styles['c-button']}
                      ${styles['c-button_secondary']}
                      ${styles['notification__button']}
                      ${styles['notification__button_secondary']}
                    `}
                    type='button'
                    onClick={toggleCookies}
                  >
                    Cookie Settings
                  </button>
                  <button
                    className={`
                      ${styles['c-button']}
                      ${styles['notification__button']}
                    `}
                    type='button'
                    onClick={acceptAction}
                  >
                    Accept All
                  </button>
                </div>
              </div>
            </div>
          )}
        </CSSTransition>
      </SwitchTransition>
    </div>
  );
};

export default MyCookies;
