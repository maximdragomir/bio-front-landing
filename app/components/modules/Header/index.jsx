import RouteLink from "../../elements/RouteLink";
import {linkGenerator} from "../../../services/linkGenerator";
import {amplitudeEvent} from "../../../libs/amplitude";

import styles from './Header.module.scss'

const Header = ({}) => {

  const tryForFreeAction = () => {
    amplitudeEvent('landing start button pressed', {place: 'header'});
    linkGenerator();
  }
  const signInAction = () => {
    amplitudeEvent('sign in pressed', {place: 'landing'});
    linkGenerator();
  }

  return (
    <header className={styles['header']}>
      <div className={`container ${styles['header__container']}`}>
        <div className={styles['header__navbar']}>
          <RouteLink link={'/'} classes={`${styles['header__logo']}`}>
            <img src={'/images/logo.svg'} alt="Lift logo"/>
          </RouteLink>
          <div className={styles['header__list']}>
            <RouteLink link={'/pricing'} activeClass={styles['header__link_active']} classes={`${styles['header__link']}`}>Pricing</RouteLink>
            <a href="https://liftstories.zendesk.com/hc/en-us" className={`${styles['header__link']}`}>Help</a>
          </div>
          <div className={styles['header__actions']}>
            <button
              type='button'
              className={`
                ${styles['c-button']}
                ${styles['c-button_secondary']}
                ${styles['header__button']}
                ${styles['header__button_secondary']}
              `}
              onClick={signInAction}
            >
              Sign in
            </button>
            <button
              type='button'
              className={`
                ${styles['c-button']}
                ${styles['header__button']}
              `}
              onClick={tryForFreeAction}
            >
              Try for free
            </button>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
