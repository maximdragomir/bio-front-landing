import {Icon} from "../../elements/Icon";

import styles from './BioSite.module.scss'

const BioSite = ({data, name, className = ''}) => {
	const {title, subtitle, background, logo, socials, buttons} = data;

  // TODO rewrite this func and take it to utils
  // if we got array convert it to string
	const getModifiers = () => {
		if(Array.isArray(name)) {
      // if "name" is array, return all classes
			return name.map((item)=> {
				return styles[`site_style-${item}`];
			}).join(' ')
		} else {
      // if "name" is string, return one class
			return styles[`site_style-${name}`];
		}
	}

	return (
		<div className={`${styles['site']} ${className} ${getModifiers()}`}>
			<picture className={styles['site__bg']}><img src={background} alt={title} /></picture>
			<picture className={styles['site__logo']}><img src={logo} alt={title} /></picture>
			<p className={styles['site__title']}>{title}</p>
			<p className={styles['site__subtitle']}>{subtitle}</p>
			<div className={`${styles['site__socials']}`}>
				{socials.map((item)=>{
					return (
						<button key={item.name} className={styles['site__social']}>
							<Icon name={item.name}/>
						</button>
					)
				})}
			</div>
			<div className={styles['site__buttons']}>
				{buttons.map((item)=>{
					return (
						<button key={item.name} className={`${styles['site__button']}`}>
							{item.image && (
								<picture className={`${styles['site__button-image']}`}>
									<img src={item.image} alt=""/>
								</picture>
							)}
							<span className={styles['site__button-text']}>{item.name}</span>
						</button>
					)
				})}
			</div>
		</div>
	);
};

export default BioSite;
