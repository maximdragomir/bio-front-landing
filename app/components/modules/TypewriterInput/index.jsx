import React from 'react';

import Cookies from "universal-cookie";
import Typewriter from "typewriter-effect";
import {Icon} from "../../elements/Icon";
import {useClickOutside} from "../../../hooks/";
import {linkGenerator} from "../../../services/linkGenerator";
import {COOKIES_DOMAIN, SITE_NAME} from "../../../utils/constants";
import {TypewriterNamesData} from "../../../data/typewriter";
import {amplitudeEvent} from "../../../libs/amplitude";

import styles from "./TypewriterInput.module.scss";


const TypewriterInput = () => {
  const cookies = new Cookies();
	// Data
	const [showInput, setShowInput] = React.useState(false);
	const [inputValue, setInputValue] = React.useState('');
	const [inputError, setInputError] = React.useState(false);
	const refInput = React.useRef();
	const typewriterInput = React.useRef();

	// Actions
	const TypewriterAction = (event) => {
		event.preventDefault();
		setShowInput(true);
	}
  const inputAction = () => {
    setInputValue(refInput?.current?.value);
  }
  const submitAction = (event) => {
    event.preventDefault();
    amplitudeEvent('landing start button pressed', {place: 'naming'});
    linkGenerator();

    // input validation
    // if(inputValue.length >= 3 && inputValue.length <= 32) {
    //   setInputError(false);
    // } else {
    //   refInput?.current?.focus();
    //   setInputError(true);
    // }
  }

	// Hooks
	useClickOutside(typewriterInput, () => {
    refInput.current?.value.length === 0 && setShowInput(false)
  });
	React.useEffect(() => {
		showInput && refInput?.current?.focus();
    setInputError(false);
	}, [showInput])
  React.useEffect(() => {
    cookies.set('liftUserSite', inputValue, { domain: COOKIES_DOMAIN });
  }, [inputValue])

	return (
		<div className={`${styles['typewriterInput']} ${inputError ? styles['typewriterInput_error'] : ''}`} ref={typewriterInput} onClick={TypewriterAction}>
			<div className={styles['typewriterInput__wrapper']}>
				<span className={styles['typewriterInput__text']}>{SITE_NAME}/</span>
				{showInput ? (
          <input
            type='text'
            ref={refInput}
            value={inputValue}
            onChange={inputAction}
            className={`${styles['typewriterInput__input']}`}
            autoFocus
          />
				) : (
					<Typewriter
						options={{
							strings: TypewriterNamesData,
							autoStart: true,
							loop: true,
							wrapperClassName: styles['typewriterInput__typewriter-text'],
							cursorClassName: styles['typewriterInput__typewriter-cursor'],
						}}
					/>
				)}
			</div>
			<button className={`${styles['c-button']} ${styles['typewriterInput__button']}`} onClick={submitAction}>
				<Icon name='arrow' className={styles['typewriterInput__button-icon']}/>
			</button>
		</div>
	);
};

export default TypewriterInput;
