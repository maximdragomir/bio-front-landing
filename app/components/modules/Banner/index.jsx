import BioSite from "../BioSite";
import TypewriterInput from "../TypewriterInput";
import {linkGenerator} from "../../../services/linkGenerator";
import {bannerBioSitesData} from "../../../data/banner";

import styles from "./Banner.module.scss";
import {amplitudeEvent} from "../../../libs/amplitude";


const Banner = ({type, ...props}) => {

  // Actions
  const bannerAction = () => {
    amplitudeEvent('landing start button pressed', {place: 'button 5'});
    linkGenerator();
  }

  // Renders
  const renderContent = (type) => {
    switch (type) {
      case 'bottom':
        return (
          <>
            <div className={`${styles['banner__info']}`}>
              <h2 className={`${styles['banner__title']}`}>Create your first site in a minute</h2>
              <p className={`${styles['banner__subtitle']}`}>All your profiles and other important information in one fully customizable and shareable link</p>
              <button className={`${styles['c-button']} ${styles['banner__button']}`} onClick={bannerAction} type='button'>Start for FREE</button>
            </div>
            <div className={`${styles['banner__decor']}`}>
              {bannerBioSitesData.map((item, index)=>(
                <BioSite key={index} data={item} name={['banner', `banner${index + 1}`]} className={styles['banner__bio-site']} />
              ))}
            </div>
          </>
        )

      default:
        return (
          <div className={`${styles['banner__info']}`}>
            <h2 className={`${styles['banner__title']}`}>Start with naming your url</h2>
            <TypewriterInput />
          </div>
        )
    }
  }

  return (
    <div
      className={`${styles['banner']} ${styles['banner_'+ type]}`}
      {...props}
    >
      {renderContent(type)}
    </div>
  );
};

export default Banner;
