import Link from "next/link";
import RouteLink from "../../elements/RouteLink";
import styles from './Footer.module.scss'

const Footer = ({}) => {

  return (
    <footer className={`${styles['footer']}`}>
      <div className={'container'}>
        <div className={`row ${styles['footer__row']}`}>
          <div className={`${styles['footer__column']} ${styles['footer__column_big']}`}>
            <Link href={'/'}>
              <a className={styles['footer__logo']}><img src={'/images/logo.svg'} alt="Lift logo"/></a>
            </Link>
            <div className={`${styles['footer__copyright']} ${styles['footer__copyright_desktop']}`}>LiftBio© 2021 All
              rights reserved
            </div>
          </div>
          <div className={styles['footer__column']}>
            <div className={`${styles['footer__title']}`}>Company</div>
            <RouteLink link={'/pricing'} classes={`${styles['footer__item']}`}>Pricing</RouteLink>
            <a href={'https://liftstories.zendesk.com/hc/en-us'} className={`${styles['footer__item']}`}>Help</a>
            <RouteLink link={'/contacts'} classes={`${styles['footer__item']}`}>Contacts</RouteLink>
          </div>
          <div className={styles['footer__column']}>
            <div className={`${styles['footer__title']}`}>Legal</div>
            <RouteLink link={'/terms'} classes={`${styles['footer__item']}`}>Terms and Service</RouteLink>
            <RouteLink link={'/privacy'} classes={`${styles['footer__item']}`}>Privacy Policy</RouteLink>
            <RouteLink link={'/subscription'} classes={`${styles['footer__item']}`}>Subscription policy</RouteLink>
            <RouteLink link={'/cookie'} classes={`${styles['footer__item']}`}>Cookie Policy</RouteLink>
          </div>
          <div className={`${styles['footer__column']} ${styles['footer__column_mobile']}`}>
            <div className={`${styles['footer__copyright']}`}>LiftBio© 2021 All rights reserved</div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
