import {useAos} from "../../../hooks/useAos";

import styles from './Privacy.module.scss';
import RouteLink from "../../elements/RouteLink";

const Privacy = ({}) => {
  useAos();

  return (
    <section className={`section terms ${styles['privacy']}`} data-aos={'fade-up'} data-aos-delay={'100'}>
      <div className={`container container_text`}>

        <h2 className={'title'}>Important privacy information</h2>
        <p className="info-text">
          We will automatically collect from your device language settings, IP address, time zone, type and model of a
          device, device settings, operating system, Internet service provider, mobile carrier, hardware ID, and other
          unique identifiers (such as IDFA and AAID). We need this data to provide our services, analyze how our
          customers use the service and to measure ads.
        </p>

        <p className="info-text">
          For improving the service and serving ads, we use third party solutions. As a result, we may process data
          using solutions developed <b className="inline">by Amazon, Apple, Amplitude, Appsflyer, Google, Firebase,
          Twitter, TikTok, Pinterest</b>. Therefore, some of the data is stored and processed on the servers of such
          third parties. This enables us to (1) analyze different interactions (what App's features our users viewed);
          (2) serve and measure ads (and show them only to a particular group of users, for example, only to
          subscribers).
        </p>
        <p className="info-text">
          Please read our Privacy Policy below to know more about what we do with data (<a href="#contents-3">Section
          3</a>), what data privacy rights are available to you (<a href="#contents-6">Section 6</a>) and who will
          be the data controller (<a href="#contents-1">Section 1</a>). If any questions will remain unanswered,
          please contact us at <a href="mailto:support@liftstory.com">support@liftstory.com</a>.
        </p>

        <h1 className={'title'}>Privacy Policy</h1>
        <p className="info-text">
          This Privacy Policy explains what personal data is collected when you use Lift: Story Maker mobile application
          (the "<b>App</b>"), the website located at: https://liftstory.com (the "<b>Website</b>"), the services and
          products provided through them (together with the App and Website, the "<b>Service</b>"), how such personal
          data will be processed.
        </p>

        <p className="info-text">
          BY USING THE SERVICE, YOU PROMISE US THAT (I) YOU HAVE READ, UNDERSTAND AND AGREE TO THIS PRIVACY POLICY,
          AND (II) YOU ARE OVER 16 YEARS OF AGE (OR HAVE HAD YOUR PARENT OR GUARDIAN READ AND AGREE TO THIS PRIVACY
          POLICY FOR YOU). If you do not agree, or are unable to make this promise, you must not use the Service. In
          such case, you must (a) contact us and request deletion of your data; (b) cancel any subscriptions using the
          functionality provided by Apple (if you are using iOS) or Google (if you are using Android), any other app
          stores that may be available from time to time; and (c) delete the App from your devices.
        </p>
        <p className="info-text">
          "<b>GDPR</b>" means the General Data Protection Regulation (EU) 2016/679 of the European Parliament and of
          the Council of 27 April 2016 on the protection of natural persons with regard to the processing of
          personal data and on the free movement of such data.
        </p>
        <p className="info-text">
          "<b>EEA</b>" includes all current member states to the European Union and the European Free Trade
          Association. For the purpose of this policy EEA shall include the United Kingdom of Great Britain and
          Northern Ireland.
        </p>
        <p className="info-text">
          "<b>Process</b>", in respect of personal data, includes to collect, store, and disclose to others.
        </p>

        <h4>TABLE OF CONTENTS</h4>

        <ul>
          <li><a href="#contents-1">1. PERSONAL DATA CONTROLLER</a></li>
          <li><a href="#contents-2">2. CATEGORIES OF PERSONAL DATA WE COLLECT</a></li>
          <li><a href="#contents-3">3. FOR WHAT PURPOSES WE PROCESS PERSONAL DATA</a></li>
          <li><a href="#contents-4">4. UNDER WHAT LEGAL BASES WE PROCESS YOUR PERSONAL DATA (Applies only to EEA-based
            users)</a></li>
          <li><a href="#contents-5">5. WITH WHOM WE SHARE YOUR PERSONAL DATA</a></li>
          <li><a href="#contents-6">6. HOW YOU CAN EXERCISE YOUR PRIVACY RIGHTS</a></li>
          <li><a href="#contents-7">7. AGE LIMITATION</a></li>
          <li><a href="#contents-8">8. INTERNATIONAL DATA TRANSFERS</a></li>
          <li><a href="#contents-9">9. CHANGES TO THIS PRIVACY POLICY</a></li>
          <li><a href="#contents-10">10. CALIFORNIA PRIVACY RIGHTS</a></li>
          <li><a href="#contents-11">11. DATA RETENTION</a></li>
          <li><a href="#contents-12">12. HOW "DO NOT TRACK" REQUESTS ARE HANDLED</a></li>
          <li><a href="#contents-13">13. CONTACT US</a></li>
        </ul>

        <h4 id="contents-1">1. PERSONAL DATA CONTROLLER</h4>

        <p className={'info-text'}>App Limited, a company registered under the laws of the Republic of Cyprus having its
          mailing address office
          at Vavylonos, 3, Latsia, 2237, Nicosia, Cyprus, will be the controller of your personal data.</p>

        <h4 id="contents-2">2. CATEGORIES OF PERSONAL DATA WE COLLECT</h4>

        <p className="info-text">We collect data you give us voluntarily (for example, email address or name)
          when reaching out to our support. We also collect data automatically (for example, your IP address).</p>

        <div className="info-text">
          <b>2.1. Data you give us</b>
          You can provide us with your email or name when you contact our support team.
        </div>

        <div className="info-text">
          <b className="info-text">2.2. Data we collect automatically:</b>

          <div className="info-text">
            <b>2.2.1. Data about how you found us</b>
            We collect data about your referring app or URL (that is, the app or place on the Web where you were when
            you tapped/clicked on our ad).
          </div>
          <div className="info-text">
            <b>2.2.2. Device and Location data</b>
            We collect data from your mobile device. Examples of such data include: language settings, IP address,
            time zone, type and model of a device, device settings, operating system, Internet service provider,
            mobile carrier and hardware ID.
          </div>
          <div className="info-text">
            <b>2.2.3. Usage data</b>
            We record how you interact with our Service. For example, we log what pages you have viewed, the features
            and content you interact with, how often you use the Service, how long you are on the Service, your
            purchases.
          </div>
          <div className="info-text">
            <b>2.2.4. Advertising IDs</b>
            We collect your Apple Identifier for Advertising ("IDFA"), Identifier for Vendor ("IDFV") or Google
            Advertising ID ("AAID") (depending on the operating system of your device) when you access our App or
            Website from a mobile device. You can typically reset these numbers through the settings of your device's
            operating system (but we do not control this).
          </div>
        </div>

        <h4 id="contents-3">3. FOR WHAT PURPOSES WE PROCESS YOUR PERSONAL DATA</h4>
        <p className="info-text">We process your personal data: </p>

        <div className="info-text">
          <p className="info-text">
            <b>3.1. To provide our Service</b> {' '}
            This includes enabling you to use the Service in a seamless manner and preventing or addressing Service
            errors
            or technical issues.
          </p>
          <p className="info-text">
            To host personal data and enable our Service to operate and be distributed we use <b className="inline">Amazon
            Web Services</b>, which is a hosting and backend service provided by Amazon.
          </p>
          <p className="info-text">
            To monitor infrastructure and the Service's performance, we use <b className="inline">Crashlytics</b>,
            which is a monitoring service provided by Google. <a
            href="https://docs.fabric.io/apple/fabric/data-privacy.html#crashlytics">Data Collection Policy</a>.
          </p>
        </div>

        <div className="info-text">
          <b>3.2. To customize your experience</b> {' '}
          We process your personal data to adjust the content of the Service and provide content tailored to your
          personal preferences
        </div>

        <div className="info-text">
          <b>3.3. To provide you with customer support</b> {' '}
          We process your personal data to respond to your requests for technical support, Service information or to any
          other communication you initiate. For this purpose, we may send you, for example, notifications or emails
          about, the performance of our Service, security, payment transactions,
          notices regarding our <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink> or this
          Privacy Policy.
        </div>

        <div className="info-text">
          <p className="info-text">
            <b>3.4. To communicate with you regarding your use of our Service</b>
            We communicate with you, for example, by push notifications. These may include notification about new
            stories
            templates arrival, or other information about the App. To opt out of receiving push notifications, you need
            to
            change the settings on your device.
          </p>
          <p className="info-text">
            The services that we use for these purposes may collect data concerning the date and time when the message
            was viewed by our Service's users, as well as when they interacted with it, such as by tapping on links
            included in the message.
          </p>
          <p className="info-text">
            We use Apple Push Notification service ("APNs"), that is a notifications service provided by Apple.
            APNs allows us to send information to iOS devices. <a href="https://www.apple.com/privacy/">Apple's
            privacy policy</a>.
          </p>
        </div>

        <div className="info-text">
          <p className="info-text">
            <b>3.5. To research and analyze your use of the Service</b>
            This helps us to better understand our business, analyze our operations, maintain, improve, innovate, plan,
            design, and develop the Service and our new products. We also use such data for statistical analysis
            purposes,
            to test and improve our offers. This enables us to better understand what categories of users use our
            Services. As a consequence, we often decide how to improve the Service based on the results obtained from
            this
            processing. For example, if we discover that users choose certain category of templates for stories less
            frequently, we may focus on improving the category or decide to remove it.
          </p>
          <p className="info-text">
            To perform research and analysis about how users interact with our Service we use <b
            className="inline">Appsflyer</b>. Appsflyer enables us to understand, in particular, how users find us
            (for example, who was the advertiser that delivered an ad to users, which led you to our Website).
            Appsflyer also provides us with different analytics tools that enable us to research and analyze your use
            of the Service.
            <a href="https://www.appsflyer.com/privacy-policy/">Privacy Policy</a>.
            Appsflyer allows you to <a href="https://www.appsflyer.com/optout">Opt Out</a> of having data from my
            device sent to Appsflyer's servers for apps usage collection.
          </p>
          <p className="info-text">
            We use <b className="inline">Facebook Analytics</b>, which is a service provided by Facebook that allows
            us to use different analytical tools. On Facebook Analytics we get, in particular, aggregated
            demographics and insights on how many people visit our Service, how often users make purchases, and
            other interactions. Lean more about Facebook's approach to data from its <a
            href="https://www.facebook.com/about/privacy/">Privacy Policy</a>.
          </p>
          <p className="info-text">
            <b className="inline">Amplitude</b> is an analytics service provided by Amplitude Inc. We use this
            tool to understand how customers use our Service. Amplitude collects various technical information, in
            particular, time zone, type of device (phone, tablet or laptop), unique identifiers (including
            advertising identifiers). Amplitude also allows us to track various interactions that occur in our
            Service. As a result, Amplitude helps us to decide what features we should focus on. Amplitude
            provides more information on how they process data in its <a href="https://amplitude.com/privacy">Privacy
            Policy</a>.
          </p>
          <p className="info-text">
            We also use <b className="inline">Firebase Analytics</b>, which is an analytics service provided by
            Google. In order to understand Google's use of data, consult Google's
            <a href="https://policies.google.com/technologies/partner-sites?hl=ru">partner policy</a>.
            Firebase <a href="https://firebase.google.com/support/privacy/">Privacy information</a>.
            <a href="https://policies.google.com/privacy">Google's Privacy Policy</a>.
          </p>
        </div>

        <div className="info-text">
          <b>3.6. To send you marketing communications</b>
          We process your personal data for our marketing campaigns. As a result, you will receive information about our
          products, such as, for example, special offers or new features available on the App. We may show you
          advertisements on our App, and send you push notifications for marketing purposes. To opt out of receiving
          push notifications from us, you need to change the settings on your device.
        </div>

        <div className="info-text">
          <p className="info-text">
            <b>3.7. To personalize our ads</b>
            We and our partners use your personal data to tailor ads and possibly even show them to you at the relevant
            time. For example, if you visited our Website or installed the App, you might see ads of our products in
            your
            Facebook's feed.
          </p>
          <p className="info-text">
            <b>How to opt out or influence personalized advertising</b>
            <b className="inline">iOS</b>: On your iPhone or iPad, go to "Settings," then "Privacy" and tap
            "Advertising" to select "Limit Ad Track". In addition, you can reset your advertising identifier (this also
            may help you to see less of personalized ads) in the same section.
          </p>
          <p className="info-text">
            <b className="inline">Android</b>: To opt-out of ads on an Android device, simply open the Google Settings
            app on your mobile phone, tap "Ads" and enable "Opt out of interest-based ads". In addition, you can reset
            your advertising identifier in the same section (this also may help you to see less of personalized ads).
            To learn even more about how to affect advertising choices on various devices, please look at the
            information available <a href="http://www.networkadvertising.org/mobile-choice">here</a>.
          </p>
          <p className="info-text">
            <b className="inline">macOS</b>: On your MacBook, you can enable Limit Ad Tracking by choosing Apple
            menu > System Preferences > Security & Privacy, clicking Privacy, and selecting the Limit Ad Tracking
            checkbox.
          </p>
          <p className="info-text">
            <b className="inline">Windows</b>: On your laptop running Windows 10, you shall select Start >
            Settings > Privacy and then turn off the setting for Let apps use advertising ID to make ads more
            interesting to you based on your app activity. If you have other Windows version, please follow the
            steps <a href="https://account.microsoft.com/privacy/ad-settings/signedout?lang=en-GB">here</a>.
          </p>
          <p className="info-text">
            To learn even more about how to affect advertising choices on various devices, please look at the
            information available <a href="http://www.networkadvertising.org/mobile-choice">here</a>.
          </p>
          <p className="info-text">
            In addition, you may get useful information and opt out of some interest-based advertising, by
            visiting the following links:
          </p>
          <p className="info-text">
            - Network Advertising Initiative - <a
            href="http://optout.networkadvertising.org/">http://optout.networkadvertising.org/</a>
          </p>
          <p className="info-text">
            - Digital Advertising Alliance - <a
            href="http://optout.aboutads.info/">http://optout.aboutads.info/</a>
          </p>
          <p className="info-text">
            - Digital Advertising Alliance (Canada) - <a
            href="http://youradchoices.ca/choices">http://youradchoices.ca/choices</a>
          </p>
          <p className="info-text">
            - Digital Advertising Alliance (EU) - <a
            href="http://www.youronlinechoices.com/">http://www.youronlinechoices.com/</a>
          </p>
          <p className="info-text">
            - DAA AppChoices page - <a
            href="http://www.aboutads.info/appchoices">http://www.aboutads.info/appchoices</a>
          </p>

          <p className="info-text">
            We value your right to influence the ads that you see, thus we are letting you know
            what service providers we use for this purpose and how some of them allow you to
            control your ad preferences.
          </p>
          <p className="info-text">
            We use <b className="inline">Facebook Ads Manager</b> together with <b
            className="inline">Facebook Custom Audience</b>, which allows us to choose
            audiences that will see our ads on Facebook or other Facebook's products (for
            example, Instagram). Through Facebook Custom Audience we may create a list of
            users with certain sets of data, such as an IDFA, choose users that have completed
            certain actions in the Service (for example, visited certain sections of the
            Service). As a result, we may ask Facebook to show some ads to a particular list
            of users. As a result, more of our ads may show up while you are using Facebook or
            other Facebook's products (for example, Instagram). You may learn how to opt out
            of advertising provided to you through Facebook Custom Audience <a
            href="https://www.facebook.com/help/568137493302217">here</a>.
          </p>
          <p className="info-text">
            Facebook also allows its users to influence the types of ads they see on
            Facebook. To find how to control the ads you see on Facebook, please go <a
            href="https://www.facebook.com/help/146952742043748?helpref=related">here</a> or
            adjust your ads settings on <a
            href="https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen">Facebook</a>.
          </p>
          <p className="info-text">
            <b className="inline">Google Ads</b> is an ad delivery service provided by
            Google that can deliver ads to users. In particular, Google allows us to
            tailor the ads in a way that they will appear, for example, only to users that
            have conducted certain actions with our Service (for example, show our ads to
            users who have made a purchase). Some other examples of events that may be
            used for tailoring ads include, in particular, visiting our Website. Google
            allows its users to <a
            href="https://adssettings.google.com/authenticated?hl=ru">opt out of Google's
            personalized ads</a> and to <a
            href="https://tools.google.com/dlpage/gaoptout/">prevent their data from being
            used by Google Analytics</a>.
          </p>
          <p className="info-text">
            <b className="inline">TikTok Ads</b> is the service provided by TikTok that
            can deliver ads to its users. The ads can be tailored to specific categories
            of users (for instance, based on their geographical location). <a
            href="https://ads.tiktok.com/i18n/official/policy/privacy">TikTok's Privacy
            Policy</a>.
          </p>
          <p className="info-text">
            We also use <b className="inline">Twitter Ads</b> provided by Twitter to
            deliver advertising. Twitter Ads allows us to choose specific audiences
            based on geographic areas or user's interests. As a result, we may ask
            Twitter to deliver our ads to certain list of users. Twitter allows you <a
            href="https://help.twitter.com/en/safety-and-security/privacy-controls-for-tailored-ads">to
            opt-out its internet-based advertising</a>. <a
            href="https://twitter.com/en/privacy">Privacy Policy</a>.
          </p>
          <p className="info-text">
            We use <b className="inline">Pinterest Ads</b> to deliver group-based
            advertisements. For example, you may see our ads if you are interested
            in specific services, information, or offers. <a
            href="https://policy.pinterest.com/en/privacy-policy">Pinterest Privacy
            Policy</a>.
          </p>
        </div>

        <div className="info-text">
          <b>3.8. To enforce our Terms and Conditions of Use and to prevent and combat fraud</b> {' '}
          We use personal data to enforce our agreements and contractual commitments, to detect, prevent, and combat
          fraud. As a result of such processing, we may share your information with others, including law enforcement
          agencies (in particular, if a dispute arises in connection with our
          <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink>).
        </div>

        <div className="info-text">
          <b>3.9. To comply with legal obligations</b> {' '}
          We may process, use, or share your data when the law requires it, in particular, if a law enforcement agency
          requests your data by available legal means.
        </div>

        <h4 id="contents-4">4. UNDER WHAT LEGAL BASES WE PROCESS YOUR PERSONAL DATA </h4>

        <p className="info-text">
          In this section, we are letting you know what legal basis we use for each particular purpose of processing.
          For more information on a particular purpose, please refer to <a href="#contents-3">Section 3</a>. This
          section applies only to EEA-based users.
          We process your personal data under the following legal bases:
        </p>

        <div className="info-text">
          <b className="inline">4.1.</b> your consent
        </div>

        <div className="info-text">
          <b className="inline">4.2.</b> to perform our contract with you;
          <ul>
            <li>Under this legal basis we:</li>
            <li>- Provide our Service (in accordance with our <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink>)
            </li>
            <li>- Customize your experience</li>
            <li>- Provide you with customer support</li>
            <li>- Communicate with you regarding your use of our Service</li>
            <li>- Process your payments</li>
          </ul>
        </div>

        <div className="info-text">
          <b className="inline">4.3.</b> for our (or others') legitimate interests, unless those interests are
          overridden by your interests or fundamental rights and freedoms that require protection of personal data;
          <ul>
            <li>We rely on legitimate interests:</li>
            <li>- to communicate with you regarding your use of our Service</li>
            <li>This includes, for example, sending you push notification reminding you to use the App. The legitimate
              interest we rely on for this purpose is our interest to encourage you to use our Service more often.
            </li>
            <li>- to research and analyze your use of the Service</li>
            <li>Our legitimate interest for this purpose is our interest in improving our Service so that we understand
              users'
              preferences and are able to provide you with a better experience (for example, to make the use of the
              Service
              easier and more enjoyable, or to introduce and test new features).
            </li>
            <li>- to send you marketing communications</li>
            <li>The legitimate interest we rely on for this processing is our interest to promote our Service, including
              new
              products and special offers, in a measured and appropriate way.
            </li>
            <li>- to personalize our ads</li>
            <li>The legitimate interest we rely on for this processing is our interest to promote our Service in a
              reasonably
              targeted way.
            </li>
            <li>- to enforce our <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink> and to
              prevent and combat fraud
            </li>
            <li>Our legitimate interests for this purpose are enforcing our legal rights, preventing and addressing
              fraud and
              unauthorised use of the Service, non-compliance with our <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink>.
            </li>
          </ul>
        </div>

        <div className="info-text">
          <b className="inline">4.4.</b> to comply with legal obligations.
        </div>

        <h4 id="contents-5">5. WITH WHOM WE SHARE YOUR PERSONAL DATA</h4>

        <p className="info-text">
          We share information with third parties that help us operate, provide, improve, integrate, customize, support,
          and market our Service. We may share some sets of personal data, in particular, for purposes and with parties
          indicated in <a href="#contents-3">Section 3</a> of this Privacy Policy. The types of third parties we share
          information with include, in particular: </p>

        <div className="info-text">
          <b>5.1. Service providers</b>
          <ul>
            <li>
              We share personal data with third parties that we hire to provide services or perform business functions
              on
              our behalf, based on our instructions. We may share your personal information with the following types of
              service providers:
            </li>
            <li>- cloud storage providers (Amazon)</li>
            <li>- data analytics providers (Facebook, Firebase, Appsflyer, Amplitude)</li>
            <li>- measurement partners</li>
            <li>- communication service providers (Apple)</li>
            <li>- marketing partners (in particular, social media networks, marketing agencies; Facebook, Google,
              Pinterest,
              TikTok, Twitter)
            </li>
          </ul>
        </div>

        <div className="info-text">
          <b>5.2. Law enforcement agencies and other public authorities</b> {' '}
          We may use and disclose personal data to enforce our <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink>, to protect our rights, privacy, safety, or property, and/or that of our affiliates,
          you or others, and to respond to requests from courts, law enforcement agencies, regulatory agencies, and
          other public and government authorities, or in other cases provided for by law.
        </div>

        <div className="info-text">
          <b>5.3. Third parties as part of a merger or acquisition</b>{' '}
          As we develop our business, we may buy or sell assets or business offerings. Customers' information is
          generally one of the transferred business assets in these types of transactions. We may also share such
          information with any affiliated entity (e.g. parent company or subsidiary) and may transfer such information
          in the course of a corporate transaction, such as the sale of our business, a divestiture, merger,
          consolidation, or asset sale, or in the unlikely event of bankruptcy.
        </div>

        <h4 id="contents-6">6. HOW YOU CAN EXERCISE YOUR PRIVACY RIGHTS</h4>

        <p className="info-text">
          To be in control of your personal data, you have the following rights:
        </p>
        <p className="info-text">
          <b className="inline">Accessing / reviewing / updating / correcting your personal data</b>. You may review,
          edit, or change the personal data that you had previously provided on the App.
        </p>
        <p className="info-text">
          <b className="inline">Deleting your personal data</b>. You can request erasure of your personal data as
          permitted by law.
        </p>
        <p className="info-text">
          When you request deletion of your personal data, we will use reasonable efforts to honor your request.
          In some cases, we may be legally required to keep some of the data for a certain time; in such event, we
          will fulfill your request after we have complied with our obligations.
        </p>
        <p className="info-text">
          <b className="inline">Objecting to or restricting the use of your personal data</b>. You can ask us to
          stop using all or some of your personal data or limit our use thereof.
        </p>
        <p className="info-text">
          <b className="inline">Additional information for EEA-based users:</b>
        </p>
        <p className="info-text">
          If you are based in the EEA, you have the following rights in addition to the above:
        </p>
        <p className="info-text">
          The right to lodge a complaint with supervisory authority. We would love you to contact us
          directly, so we could address your concerns. Nevertheless, you have the right to lodge a
          complaint with a competent data protection supervisory authority, in particular in the EU Member
          State where you reside, work or where the alleged infringement has taken place.
        </p>
        <p className="info-text">
          The right to data portability. If you wish to receive your personal data in a machine-readable
          format, you can send respective request to us as described below.
        </p>
        <p className="info-text">
          To exercise any of your privacy rights, please send a request to <a
          href="mailto:support@liftstory.com">support@liftstory.com</a>.
        </p>

        <h4 id="contents-7">7. AGE LIMITATION</h4>

        <p className={'info-text'}>We do not knowingly process personal data from persons under 16 years of age. If you
          learn that anyone
          younger than 16 has provided us with personal data, please contact us.</p>

        <h4 id="contents-8">8. INTERNATIONAL DATA TRANSFERS</h4>

        <p className="info-text">
          We may transfer personal data to countries other than the country in which the data was originally collected
          in order to provide the Service set forth in the <RouteLink link={'/terms'}>Terms and Conditions of Use</RouteLink> and for purposes indicated in this Privacy Policy. If these countries do not have the
          same data protection laws as the country in which you initially provided the information, we deploy special
          safeguards.
        </p>
        <p className="info-text">
          In particular, if we transfer personal data originating from the EEA to countries with not adequate level of
          data protection, we use one of the following legal bases: (i) Standard Contractual Clauses approved by the
          European Commission (details available <a
          href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/model-contracts-transfer-personal-data-third-countries_en">here</a>),
          or (ii) the European Commission adequacy decisions about certain countries (details available <a
          href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/adequacy-protection-personal-data-non-eu-countries_en">here</a>).
        </p>

        <h4 id="contents-9">9. CHANGES TO THIS PRIVACY POLICY</h4>

        <p className={'info-text'}>We may modify this Privacy Policy from time to time. If we decide to make material
          changes to this Privacy
          Policy, you will be notified by available means such as email and will have an opportunity to review the
          revised Privacy Policy. By continuing to access or use the Service after those changes become effective, you
          agree to be bound by the revised Privacy Policy.</p>

        <h4 id="contents-10">10. CALIFORNIA PRIVACY RIGHTS</h4>

        <p className="info-text">
          California's Shine the Light law gives California residents the right to ask companies once a year what
          personal information they share with third parties for those third parties' direct marketing purposes. Learn
          more about what is considered to be <a
          href="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=1798.81.5">personal
          information under the statute</a>.
        </p>
        <p className="info-text">
          To obtain this information from us, please send an email message to <a
          href="mailto:support@liftstory.com">support@liftstory.com</a> which includes "Request for California Privacy
          Information" on the subject line and your state of residence and email address in the body of your message.
          If you are a California resident, we will provide the requested information to you at your email address in
          response.
        </p>

        <h4 id="contents-11">11. DATA RETENTION</h4>

        <p className="info-text">
          We will store your personal data for as long as it is reasonably necessary for achieving the purposes set
          forth in this Privacy Policy (including providing the Service to you). We will also retain and use your
          personal data as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
        </p>

        <h4 id="contents-12">12. HOW "DO NOT TRACK" REQUESTS ARE HANDLED</h4>

        <p className="info-text">
          Except as otherwise stipulated in this Privacy Policy, this Service does not support "Do Not Track" requests.
          To determine whether any of the third-party services it uses honor the "Do Not Track" requests, please read
          their privacy policies.
        </p>

        <h4 id="contents-13">13. CONTACT US</h4>

        <p className="info-text">
          You may contact us at any time for details regarding this Privacy Policy and its previous versions. For any
          questions concerning your account or your personal data please contact us at <a
          href="mailto:support@liftstory.com">support@liftstory.com</a>.
        </p>

        <b className="date">Effective as of: 24 November 2020</b>
      </div>
    </section>
  );
};

export default Privacy;


