import styles from './Terms.module.scss';
import {useAos} from "../../../hooks/useAos";
import RouteLink from "../../elements/RouteLink";

const Terms = ({}) => {
  useAos();

  return (
    <section className={`section terms ${styles['terms']}`} data-aos={'fade-up'} data-aos-delay={'100'}>
      <div className={`container container_text`}>
        <h1 className="title">Terms of service</h1>
        <p className={`subtitle ${styles['terms__subtitle']}`}>Last Updated: November, 2020</p>

        <p className="info-text">
          <b>THIS SERVICE MAY INCLUDE SUBSCRIPTIONS THAT AUTOMATICALLY RENEW</b>.
          PLEASE READ THESE TERMS AND CONDITIONS OF USE TOGETHER WITH SUBSCRIPTION TERMS CAREFULLY
          BEFORE STARTING A FREE TRIAL OR COMPLETING A PURCHASE FOR AUTO-RENEWING SUBSCRIPTION SERVICE.
          <b>TO AVOID BEING CHARGED</b> YOU MUST AFFIRMATIVELY CANCEL A SUBSCRIPTION OR A FREE TRIAL IN YOUR APP STORE’S
          ACCOUNT SETTINGS <b>AT LEAST 24 HOURS BEFORE THE END OF THE FREE TRIAL OR THEN-CURRENT SUBSCRIPTION PERIOD</b>.
        </p>
        <p className="info-text">IF YOU ARE UNSURE HOW TO CANCEL A SUBSCRIPTION OR A FREE TRIAL, PLEASE VISIT THE <a
          href="https://support.apple.com/en-us/HT202039">APPLE SUPPORT WEBSITE</a>, <a
          href="https://support.google.com/googleplay/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en">GOOGLE PLAY
          HELP</a> (OR ANY OTHER APP STORES SUPPORT PAGES). DELETING THE APP DOES NOT CANCEL YOUR SUBSCRIPTIONS AND FREE
          TRIALS. YOU MAY WISH TO TAKE A SCREENSHOT OF THIS INFORMATION FOR YOUR REFERENCE.</p>

        <h4>Legally Binding Agreement; Amendments</h4>

        <ol className="info-text">
          <li>These Terms of Service ("Terms") govern your relationship with Lift ("Service" or "App") operated by
            Funplaceapp Limited ("Company", "us", "we", or "our").
          </li>
          <li>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THE SERVICE.</li>
          <li>Your access to and use of the Service is conditioned on your acceptance of and compliance with these
            Terms. These Terms apply to all visitors, users and others who access or use the Service.
          </li>
          <li>You agree that by accessing the Service, you have read, understood, and agree to be bound by all of these
            Terms. If you do not agree with all of these Terms, then you are expressly prohibited from using the Service
            and you must discontinue use immediately.
          </li>
          <li>THESE TERMS CONTAIN DISCLAIMERS OF WARRANTIES (SECTION 12), LIMITATION OF LIABILITY (SECTION 13), AS WELL
            AS PROVISIONS THAT WAIVE YOUR RIGHT TO A JURY TRIAL, RIGHT TO A COURT HEARING AND RIGHT TO PARTICIPATE IN A
            className ACTION (ARBITRATION AND className ACTION WAIVER). UNLESS YOU OPT OUT WITHIN 30 DAYS OF FIRST USE
            OF OUR SERVICE AS PROVIDED FOR IN SECTION 15, ARBITRATION IS THE EXCLUSIVE REMEDY FOR ANY AND ALL DISPUTES
            AND IS MANDATORY EXCEPT AS SPECIFIED BELOW IN SECTION 16.
          </li>
          <li>Supplemental terms, policies or documents that may be posted at the Service from time to time are hereby
            expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or
            modifications to these Terms at any time and for any reason.
          </li>
          <li>We will alert you about any changes by updating the "Last updated" date of these Terms and you waive any
            right to receive specific notice of each such change.
          </li>
          <li>It is your responsibility to periodically review these Terms to stay informed of updates. You will be
            subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised
            Terms by your continued use of the Service after the date such revised Terms are posted.
          </li>
        </ol>

        <h4>Use of Service; Age Restrictions</h4>

        <ol className="info-text">
          <li>You use the Service at your own risk and responsibility and are solely and exclusively responsible for the
            use of the Service. We will not be liable for any of your actions done using the Service.
          </li>
          <li>The Service is not intended for distribution to or use by any person or entity in any jurisdiction or
            country where such distribution or use would be contrary to law or regulation or which would subject us to
            any registration requirement within such jurisdiction or country.
          </li>
          <li>Those persons who choose to access the Service from other locations do so on their own initiative and are
            solely responsible for compliance with local laws, if and to the extent local laws are applicable.
          </li>
          <li>The Service is intended for users who are at least 13 years old and have reached the age, which allows
            them to accept these Terms on their own as established by their respective state or country of residence.
            All users who are minors in the jurisdiction in which they reside must have the permission of, and be
            directly supervised by, their parent or guardian to use the Service. If you are a minor, you must have your
            parent or guardian read and agree to these Terms prior to you using the Service.
          </li>
          <li>You acknowledge that all the text, images, marks, logos, compilations (meaning the collection, arrangement
            and assembly of information), data, other content, software and materials displayed on the Service or used
            by the Company to operate the Service (including the App and the Content and excluding any User Content (as
            defined below)) is proprietary to us or to third parties.
          </li>
          <li>The Company expressly reserves all rights, including all intellectual property rights, in all of the
            foregoing, and except as expressly permitted by these Terms, any use, redistribution, sale, decompilation,
            reverse engineering, disassembly, translation or other exploitation of them is strictly prohibited. The
            provision of the Service does not transfer to you or any third party any rights, title or interest in or to
            such intellectual property rights.
          </li>
          <li>Any data, text and other material that you may submit or post to the App ("User Content") remain your
            intellectual property, and the Company does not claim any ownership of the copyright or other proprietary
            rights in such registration information and the User Content. Notwithstanding the foregoing, you agree that
            the Company may retain copies of the User Content and the User Content as reasonably necessary for or
            incidental to its operation of the Service and as described in these Terms and the Privacy Policy.
          </li>
          <li>You grant the Company the non-exclusive, worldwide, transferable, perpetual, irrevocable right to publish,
            distribute, publicly display and perform the User Content in connection with the Service.
          </li>
          <li>Subject to these Terms, the Company grants you a non-transferable, non-exclusive, license (without the
            right to sublicense) to (i) use the Service solely for your personal, non-commercial purposes, and (b)
            install and use the App, solely on your own handheld mobile device (e.g., iOS, Android, etc. as applicable)
            and solely for your personal, non-commercial purposes.
          </li>
          <li>You agree, and represent and warrant, that your use of the Service, or any portion thereof, will be
            consistent with the foregoing license, covenants and restrictions and will neither infringe nor violate the
            rights of any other party or breach any contract or legal duty to any other parties. In addition, you agree
            that you will comply with all applicable laws, regulations and ordinances relating to the Service or your
            use of it, and you will be solely responsible for your own individual violations of any such laws.
          </li>
          <li>You are solely responsible for obtaining the equipment and telecommunication services necessary to access
            the Service, and all fees associated therewith (such as computing devices and Internet service provider and
            airtime charges).
          </li>
          <li>We retain the right to implement any changes to the Service (whether to free or paid features) at any
            time, with or without notice. You acknowledge that a variety of Company's actions may impair or prevent you
            from accessing the Service at certain times and/or in the same way, for limited periods or permanently, and
            agree that the Company has no responsibility or liability as a result of any such actions or results,
            including, without limitation, for the deletion of, or failure to make available to you, any content or
            services.
          </li>
          <li>Your access to and use of the Service is at your own risk. The Company will have no responsibility for any
            harm to your computing system, loss of data, or other harm to you or any third party, including, without
            limitation, any bodily harm, that results from your access to or use of the Service, or reliance on any
            information or advice.
          </li>
          <li>The Company has no obligation to provide you with customer support of any kind. However, the Company may
            provide you with customer support from time to time, at the Company's sole discretion.
          </li>
        </ol>

        <h4>App stores, third party ads, other users</h4>

        <ol className="info-text">
          <li>You acknowledge and agree that the availability of the App is dependent on the third party from which you
            received the App, e.g., the Apple iTunes App Store, and/or other app stores (collectively, "App Stores" and
            each, an "App Store").
          </li>
          <li>You agree to pay all fees charged by the App Stores in connection with the App. You agree to comply with,
            and your license to use the App is conditioned upon your compliance with, all applicable agreements, terms
            of use/service, and other policies of the App Stores. You acknowledge that the App Stores (and their
            subsidiaries) are a third party beneficiary of these Terms and will have the right to enforce these Terms.
          </li>
          <li>The Service may contain links to third party websites or resources and advertisements for third parties
            (collectively, "Third Party Ads"). Such Third Party Ads are not under the control of the Company and the
            Company is not responsible for any Third Party Ads. The Company provides these Third Party Ads only as a
            convenience and does not review, approve, monitor, endorse, warrant, or make any representations with
            respect to Third Party Ads. Advertisements and other information provided by Third Party Sites Ads may not
            be wholly accurate. You acknowledge sole responsibility for and assume all risk arising from your use of any
            such websites or resources. When you link to a third party site, the applicable service provider's terms and
            policies, including privacy and data gathering practices govern. You should make whatever investigation you
            feel necessary or appropriate before proceeding with any transaction with any third party. Your transactions
            and other dealings with Third Party Ads that are found on or through the App, including payment and delivery
            of related goods or services, are solely between you and such merchant or advertiser.
          </li>
          <li>Each user of the Service is solely responsible for any and all his or her User Content. Because we do not
            control the User Content, you acknowledge and agree that we are not responsible for any User Content and we
            make no guarantees regarding the accuracy, currency, suitability, or quality of any User Content, and we
            assume no responsibility for any User Content. Your interactions with other Service users are solely between
            you and such user. You agree that the Company will not be responsible for any loss or damage incurred as the
            result of any such interactions. If there is a dispute between you and any Service user, we are under no
            obligation to become involved.
          </li>
          <li>You hereby release us, our officers, employees, agents and successors from claims, demands any and all
            losses, damages, rights, claims, and actions of any kind including personal injuries, death, and property
            damage, that is either directly or indirectly related to or arises from any interactions with or conduct of
            any App Store, any other Service users, or any Third Party Ads.
          </li>
        </ol>

        <h4>Subscription fees and payment</h4>

        <ol className="info-text">
          <li>The App is free to download. However, certain features of the Service are offered on a subscription basis
            for a fee. You will pay an App Store the applicable fees (and any related taxes) as they become due.
          </li>
          <li>To the maximum extent permitted by applicable laws, we may change subscription fees at any time. We will
            give you reasonable notice of any such pricing changes by posting the new prices on or through the App
            and/or by sending you an email notification. If you do not wish to pay the new fees, you can cancel the
            applicable subscription prior to the change going into effect.
          </li>
          <li>You authorize the App Stores to charge the applicable fees to the payment card that you submit.</li>
          <li>By signing up for certain subscriptions, you agree that your subscription may be automatically renewed.
            Unless you cancel your subscription you authorize the App Stores to charge you for the renewal term. The
            period of auto-renewal will be the same as your initial subscription period unless otherwise disclosed to
            you on the Service. The renewal rate will be no more than the rate for the immediately prior subscription
            period, excluding any promotional and discount pricing, unless we notify you of a rate change prior to your
            auto-renewal. You must cancel your subscription in accordance with the cancellation procedures disclosed to
            you for the particular subscription. We will not refund fees that may have accrued to your account and will
            not prorate fees for a cancelled subscription.
          </li>
          <li>We may offer a free trial subscription for the Service. Free trial provides you access to the Service for
            a period of time, with details specified when you sign up for the offer. Unless you cancel 24 hours before
            the end of the free trial, or unless otherwise stated, your access to the Service will automatically
            continue and you will be billed the applicable fees for the Service. We may send you a reminder when your
            free trial is about to end, but we do not guarantee any such notifications. It is ultimately your
            responsibility to know when the free trial will end. We reserve the right, in our absolute discretion, to
            modify or terminate any free trial offer, your access to the Service during the free trial, or any of these
            terms without notice and with no liability. We reserve the right to limit your ability to take advantage of
            multiple free trials.
          </li>
          <li>The Service and your rights to use it expire at the end of the paid period of your subscription. If you do
            not pay the fees or charges due, we may make reasonable efforts to notify you and resolve the issue;
            however, we reserve the right to disable or terminate your access to the Service (and may do so without
            notice).
          </li>
          <li>Subscriptions purchased via an App Store are subject to such App Store's refund policies. This means we
            cannot grant refunds. You will have to contact an App Store support.
          </li>
        </ol>

        <h4>Intellectual Property Rights; License</h4>

        <ol className="info-text">
          <li>Unless otherwise indicated, the Service is our proprietary property and all source code, databases,
            functionality, software, designs, audio, video, text, photographs, and graphics at the Service
            (collectively, "Content") and the trademarks, service marks, and logos contained therein ("Marks") are owned
            or controlled by us or licensed to us, and are protected by law. The Content and the Marks are provided at
            the Service "AS IS" for your information and personal use only.
          </li>
          <li>Except as expressly provided in these Terms, no part of the Service and no Content or Marks may be copied,
            reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted,
            distributed, sold, licensed, or otherwise exploited for any commercial purpose whatsoever, without our
            express prior written permission.
          </li>
          <li>Provided that you are eligible to use the Service, we grant you a revocable, non-exclusive,
            non-transferable, limited right to install and use the Service on electronic devices owned or controlled by
            you, and to access and use the Service on such devices strictly in accordance with these Terms.
          </li>
        </ol>

        <h4>User Representations</h4>

        <div className="info-text">
          <p>By using the Service, you represent and warrant that:</p>

          <ol>
            <li>you have the legal capacity and you agree to comply with these Terms;</li>
            <li>you are at least 13 years old and have reached the age, which allows you to accept this Terms on your
              own as established by your respective state or country of residence;
            </li>
            <li>you are not a minor in the jurisdiction in which you reside, or if a minor, you have received parental
              permission to use the Service;
            </li>
            <li>you will not access the Service through automated or non-human means, whether through a bot, script or
              otherwise;
            </li>
            <li>you will not use the Service for any illegal or unauthorized purpose;</li>
            <li>you are not located in a country that is subject to a U.S. government embargo, or that has been
              designated by the U.S. government as a "terrorist supporting" country;
            </li>
            <li>you are not listed on any U.S. government list of prohibited or restricted parties; and</li>
            <li>your use of the Service will not violate any applicable law or regulation.</li>
          </ol>

          <p>If you provide any information that is untrue, inaccurate, not current, or incomplete, we have the right to
            refuse any and all current or future use of the Service (or any portion thereof).</p>
        </div>

        <h4>Prohibited Activities</h4>

        <div className="info-text">
          <p>You may not access or use the Service for any purpose other than that for which we make the Service
            available. The Service may not be used in connection with any commercial endeavors except those that are
            specifically endorsed or approved by us.
            As a user of the Service, you agree not to:</p>

          <ol>
            <li>systematically retrieve data or other content from the Service to create or compile, directly or
              indirectly, a collection, compilation, database, or directory without written permission from us;
            </li>
            <li>make any unauthorized use of the Service;</li>
            <li>make any modification, adaptation, improvement, enhancement, translation, or derivative work from the
              Service;
            </li>
            <li>use the Service for any revenue generating endeavor, commercial enterprise, or other purpose for which
              it is not designed or intended;
            </li>
            <li>make the Service available over a network or other environment permitting access or use by multiple
              devices or users at the same time;
            </li>
            <li>use the Service for creating a product, service, or software that is, directly or indirectly,
              competitive with or in any way a substitute for the Service;
            </li>
            <li>use any proprietary information or any of our interfaces or our other intellectual property in the
              design, development, manufacture, licensing, or distribution of any applications, accessories, or devices
              for use with the Service;
            </li>
            <li>circumvent, disable, or otherwise interfere with security-related features of the Service;</li>
            <li>engage in unauthorized framing of or linking to the Service;</li>
            <li>interfere with, disrupt, or create an undue burden on the Service or the networks or services connected
              to the Service;
            </li>
            <li>decipher, decompile, disassemble, or reverse engineer any of the software comprising or in any way
              making up a part of the Service;
            </li>
            <li>attempt to bypass any measures of the Service designed to prevent or restrict access to the Service, or
              any portion of the Service;
            </li>
            <li>upload or distribute in any way files that contain viruses, worms, trojans, corrupted files, or any
              other similar software or programs that may damage the operation of another’s computer;
            </li>
            <li>use, launch, develop, or distribute any automated system, including without limitation, any spider,
              robot, cheat utility, scraper, or offline reader that accesses the Service, or using or launching any
              unauthorized script or other software;
            </li>
            <li>use the Service to send automated queries to any website or to send any unsolicited commercial e-mail;
            </li>
            <li>exploit children in any way, including audio, video, photography and any other digital content;</li>
            <li>disparage, tarnish, or otherwise harm, in our opinion, us and/or the Service;</li>
            <li>use the Service in a manner inconsistent with any applicable laws or regulations; or otherwise infringe
              these Terms.
            </li>
          </ol>
        </div>

        <h4>User Data</h4>

        <ol className="info-text">
          <li>We care about data privacy and security. Please review our <RouteLink link={'/privacy'} >Privacy
            Policy</RouteLink>. It contains information that you should review prior to using the Service.
          </li>
          <li>By using the Service, you agree to be bound by the Privacy Policy, which is incorporated into these
            Terms.
          </li>
        </ol>

        <h4>Term and Termination</h4>

        <ol className="info-text">
          <li>These Terms shall remain in full force and effect while you use the Service. WITHOUT LIMITING ANY OTHER
            PROVISION OF THESE TERMS, WE RESERVE THE RIGHT TO, IN OUR SOLE DISCRETION AND WITHOUT NOTICE OR LIABILITY,
            DENY ACCESS TO AND USE OF THE SERVICE (INCLUDING BLOCKING CERTAIN IP ADDRESSES), TO ANY PERSON FOR ANY
            REASON OR FOR NO REASON, INCLUDING WITHOUT LIMITATION FOR BREACH OF ANY REPRESENTATION, WARRANTY, OR
            COVENANT CONTAINED IN THESE TERMS OR OF ANY APPLICABLE LAW OR REGULATION. WE MAY TERMINATE YOUR USE OR
            PARTICIPATION IN THE SERVICE, WITHOUT WARNING, IN OUR SOLE DISCRETION.
          </li>
          <li>In addition, we reserve the right to take appropriate legal action, including without limitation pursuing
            civil, criminal, and injunctive redress.
          </li>
        </ol>

        <h4>Modifications and Interruptions</h4>

        <ol className="info-text">
          <li>We reserve the right to change, revise, update, suspend, discontinue, or otherwise modify the Service at
            any time or for any reason without notice to you.
          </li>
          <li>We will not be liable to you or any third party for any modification, price change, suspension, or
            discontinuance of the Service.
          </li>
          <li>We cannot guarantee the Service will be available at all times. We may experience hardware, software, or
            other problems or need to perform maintenance related to the Service, resulting in interruptions, delays, or
            errors.
          </li>
          <li>You agree that we have no liability whatsoever for any loss, damage, or inconvenience caused by your
            inability to access or use the Service during any downtime or discontinuance of the Service.
          </li>
          <li>Nothing in these Terms will be construed to obligate us to maintain and support the Service or to supply
            any corrections, updates, or releases in connection therewith.
          </li>
        </ol>

        <h4>Corrections</h4>

        <p className="info-text">
          There may be information at the Service that contains typographical errors, inaccuracies, or omissions that
          may relate to the Service, including descriptions, pricing, availability, and various other information. We
          reserve the right to correct any errors, inaccuracies, or omissions and to change or update the information at
          the Service at any time, without prior notice.
        </p>

        <h4>DISCLAIMER OF WARRANTIES</h4>

        <ol className="info-text">
          <li>THE SERVICE IS PROVIDED ON AN AS-IS AND AS-AVAILABLE BASIS. YOU AGREE THAT YOUR USE OF THE SERVICE WILL BE
            AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED,
            IN CONNECTION WITH THE SERVICE AND YOUR USE THEREOF, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES
            OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
          </li>
          <li>WE MAKE NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THE SERVICE’S CONTENT OR
            THE CONTENT OF ANY WEBSITES LINKED TO THIS SERVICE AND WE WILL ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY
            (1) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT AND MATERIALS, (2) PERSONAL INJURY OR PROPERTY DAMAGE, OF
            ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE SERVICE, (3) ANY UNAUTHORIZED ACCESS TO
            OR USE OF SECURE SERVERS WE USE AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED
            THEREIN, (4) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE SERVICE, (5) ANY BUGS, VIRUSES,
            TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH THE SERVICE BY ANY THIRD PARTY, AND/OR (6)
            ANY ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A
            RESULT OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICE.
          </li>
          <li>WE DO NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR
            OFFERED BY A THIRD PARTY THROUGH THE SERVICE, ANY HYPERLINKED WEBSITE, OR ANY WEBSITE OR MOBILE APPLICATION
            FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND WE WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR
            MONITORING ANY TRANSACTION BETWEEN YOU AND ANY THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.
          </li>
          <li>AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR
            BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.
          </li>
        </ol>

        <h4>LIMITATION OF LIABILITY</h4>

        <ol className="info-text">
          <li>IN NO EVENT WILL WE OR OUR DIRECTORS, EMPLOYEES, CONTRACTORS, AFFILIATES OR AGENTS BE LIABLE TO YOU OR ANY
            THIRD PARTY FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL, OR PUNITIVE DAMAGES,
            INCLUDING LOST PROFIT, LOST REVENUE, LOSS OF DATA, OR OTHER DAMAGES ARISING FROM YOUR USE OF THE SERVICE,
            EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
          </li>
          <li>NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, OUR LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER
            AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO THE LESSER OF THE AMOUNT PAID, IF
            ANY, BY YOU TO US DURING THE SIX (6) MONTH PERIOD PRIOR TO ANY CAUSE OF ACTION ARISING OR $1,000.
          </li>
          <li>CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF
            CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS OR LIMITATIONS MAY NOT
            APPLY TO YOU, AND YOU MAY HAVE ADDITIONAL RIGHTS.
          </li>
        </ol>

        <h4>Indemnification</h4>

        <ol className="info-text">
          <li>You agree to defend, indemnify, and hold us harmless, including our subsidiaries, affiliates, and all of
            our respective officers, agents, partners, contractors and employees, from and against any loss, damage,
            liability, claim, or demand, including reasonable attorneys' fees and expenses, made by any third party due
            to or arising out of: (1) use of the Service; (2) breach of these Terms; (3) any breach of your
            representations and warranties set forth in these Terms; or (4) your violation of the rights of a third
            party, including but not limited to intellectual property rights.
          </li>
          <li>Notwithstanding the foregoing, we reserve the right, at your expense, to assume the exclusive defense and
            control of any matter for which you are required to indemnify us, and you agree to cooperate, at your
            expense, with our defense of such claims. We will use reasonable efforts to notify you of any such claim,
            action, or proceeding which is subject to this indemnification upon becoming aware of it.
          </li>
        </ol>

        <h4>MANDATORY BINDING ARBITRATION AND className ACTION WAIVER</h4>

        <ol className="info-text">
          <li>PLEASE READ THIS ARBITRATION PROVISION CAREFULLY TO UNDERSTAND YOUR RIGHTS. EXCEPT WHERE PROHIBITED BY
            LAW, YOU AGREE THAT ANY CLAIM THAT YOU MAY HAVE IN THE FUTURE MUST BE RESOLVED THROUGH FINAL AND BINDING
            CONFIDENTIAL ARBITRATION. YOU ACKNOWLEDGE AND AGREE THAT YOU ARE WAIVING THE RIGHT TO A TRIAL BY JURY. THE
            RIGHTS THAT YOU WOULD HAVE IF YOU WENT TO COURT, SUCH AS DISCOVERY OR THE RIGHT TO APPEAL, MAY BE MORE
            LIMITED OR MAY NOT EXIST.
          </li>
          <li>YOU AGREE THAT YOU MAY ONLY BRING A CLAIM IN YOUR INDIVIDUAL CAPACITY AND NOT AS A PLAINTIFF (LEAD OR
            OTHERWISE) OR className MEMBER IN ANY PURPORTED className OR REPRESENTATIVE PROCEEDING. YOU FURTHER AGREE
            THAT THE ARBITRATOR MAY NOT CONSOLIDATE PROCEEDINGS OR CLAIMS OR OTHERWISE PRESIDE OVER ANY FORM OF A
            REPRESENTATIVE OR className PROCEEDING.
          </li>
          <li>YOU AND THE COMPANY, AND EACH OF ITS RESPECTIVE AGENTS, CORPORATE PARENTS, SUBSIDIARIES, AFFILIATES,
            PREDECESSORS IN INTEREST, SUCCESSORS, AND ASSIGNS, AGREE TO ARBITRATION (EXCEPT FOR MATTERS THAT MAY BE
            TAKEN TO SMALL CLAIMS COURT), AS THE EXCLUSIVE FORM OF DISPUTE RESOLUTION EXCEPT AS PROVIDED FOR BELOW, FOR
            ALL DISPUTES AND CLAIMS ARISING OUT OF OR RELATING TO THIS AGREEMENT, THE SERVICE, OR THE PRIVACY POLICY,
            UNLESS YOU ARE LOCATED IN A JURISDICTION THAT PROHIBITS THE EXCLUSIVE USE OF ARBITRATION FOR DISPUTE
            RESOLUTION.
          </li>
          <li>Arbitration is more informal way to settle disputes than a lawsuit in court. A neutral arbitrator instead
            of a judge or jury is used in arbitration, which allows for more limited discovery than in court, and is
            subject to very limited review by courts. The same damages and relief that a court can award can be awarded
            by arbitrators. Please see more information about arbitration at <a href="http://www.adr.org"
                                                                                target="_blank">http://www.adr.org</a>.
          </li>
          <li>A party which intends to seek arbitration must first send to the other a written notice of intent to
            arbitrate (a "Notice") by an international courier with a tracking mechanism, or, in the absence of a
            mailing address provided by you to us, via any other method available to us, including via e-mail. The
            Notice to the Company must be addressed to: 116 Main Street, P.O. Box 3324, Road Town, Tortola, British
            Virgin Islands (as applicable, the "Arbitration Notice Address"). The Notice shall (i) describe the basis
            and nature of the claim or dispute; and (ii) set the specific relief sought (the "Demand"). If you and the
            Company do not reach an agreement to resolve the claim within 30 days after the Notice is received, then you
            or we may commence an arbitration proceeding as set forth below or file an individual claim in small claims
            court.
          </li>
          <li>THE AMERICAN ARBITRATION ASSOCIATION ("AAA") WILL EXCLUSIVELY ADMINISTER THE ARBITRATION IN ACCORDANCE
            WITH ITS COMMERCIAL ARBITRATION RULES AND THE SUPPLEMENTARY PROCEDURES FOR CONSUMER RELATED DISPUTES (THE
            "Rules"), AS MODIFIED BY THESE TERMS.
          </li>
          <li>If you commence arbitration against us, you are required to provide a second Notice to the Company at the
            Arbitration Notice Address within seven (7) days of arbitration commencement. The Rules and AAA forms are
            available online at <a href="http://www.adr.org" target="_blank">http://www.adr.org</a>. Unless your Demand
            is equal to or greater than $1,000 or was filed in bad faith, in which case you are solely responsible for
            the payment of the filing fee, if you are required to pay a filing fee to commence an arbitration against
            us, then we will promptly reimburse you for your confirmed payment of the filing fee upon our receipt of the
            second Notice at the Arbitration Notice Address that you have commenced arbitration along with a receipt
            evidencing payment of the filing fee.
          </li>
          <li>The arbitration shall be conducted exclusively in English. A single, independent and impartial arbitrator
            with his or her primary place of business in Alexandria, Virginia (if you are from the United States) or in
            Tortola, British Virgin Islands (if you are not from the United States) will be appointed pursuant to the
            Rules, as modified herein. You and the Company agree to comply with the following rules, which are intended
            to streamline the arbitration process and reduce the costs and burdens on the parties: (i) the arbitration
            will be conducted online and/or be solely based on written submissions, the specific manner to be chosen by
            the party initiating the arbitration; (ii) the arbitration will not require any personal appearance by the
            parties or witnesses unless otherwise mutually agreed in writing by the parties; and (iii) any judgment on
            the award the arbitrator renders may be entered in any court of competent jurisdiction.
          </li>
          <li>TO THE FULLEST EXTENT PERMITTED UNDER LAW, YOU AND THE COMPANY AGREE THAT YOU AND THE COMPANY MAY BRING
            CLAIMS AGAINST THE OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITY AND NOT AS A PLAINTIFF OR className MEMBER
            IN ANY PURPORTED className, REPRESENTATIVE, OR CONSOLIDATED PROCEEDING. FURTHER, YOU AGREE THAT THE
            ARBITRATOR MAY NOT CONSOLIDATE PROCEEDINGS OF MORE THAN ONE PERSON’S CLAIMS, AND MAY NOT OTHERWISE PRESIDE
            OVER ANY FORM OF A REPRESENTATIVE OR className PROCEEDING, AND THAT IF THIS SPECIFIC PROVISION IS FOUND TO
            BE UNENFORCEABLE, THEN THE ENTIRETY OF THIS MANDATORY ARBITRATION SECTION WILL BE NULL AND VOID.
          </li>
          <li>The arbitrator shall have the exclusive and sole authority to resolve any dispute relating to the
            interpretation, construction, validity, applicability, or enforceability of these Terms, the Privacy Policy,
            and this arbitration provision. The arbitrator shall have the exclusive and sole authority to determine
            whether this arbitration clause can be enforced against a non-party to this agreement and whether a
            non-party to these Terms can enforce its provision against you or us.
          </li>
          <li>Barring extraordinary circumstances, the arbitrator will issue his or her final, confidential decision
            within 120 days from the date the arbitrator is appointed. The arbitrator may extend this time limit for an
            additional 30 days upon a showing of good cause and in the interests of justice. All arbitration proceedings
            will be closed to the public and confidential, and all records relating thereto will be permanently sealed,
            except as necessary to obtain court confirmation of the arbitration award. The award of the arbitrator will
            be in writing and will include a statement setting forth the reasons for the disposition of any claim. The
            arbitrator shall apply the laws of the Commonwealth of Virginia without regard to its conflicts of laws
            principles in conducting the arbitration. You acknowledge that these terms and your use of the Service
            evidences a transaction involving interstate commerce. The United States Federal Arbitration Act ("FAA")
            will govern the interpretation, enforcement, and proceedings pursuant to this Section 15. Any award rendered
            shall be final, subject to appeal under the FAA.
          </li>
          <li>The abovestated provisions of this Section 15 shall not apply to any claim in which either party seeks
            equitable relief to protect such party's copyrights, trademarks, patents, or other intellectual property.
            For the avoidance of doubt, you agree that, in the event the Company or a third party breaches these Terms,
            the damage or harm, if any, caused to you will not entitle you to seek injunctive or other equitable relief
            against us, and your only remedy will be for monetary damages, subject to the limitations of liability set
            forth in these Terms.
          </li>
          <li>You and we agree that, notwithstanding any other rights a party may have at law or in equity, any claim
            arising out of or related to these Terms (including the Privacy Policy) or the Service, excluding a claim
            for indemnification, must be initiated with the AAA or filed in small claims court in Alexandria, Virginia
            within one (1) year after the claim accrues. Otherwise, such cause of action is permanently and forever
            barred. This one (1) year period includes the thirty (30) day pre-dispute procedure set forth in sub-section
            15.5 above.
          </li>
          <li>All claims you bring against the Company must be resolved in accordance with this Section. All claims
            filed or brought contrary to this Section shall be considered improperly filed. Should you file a claim
            contrary to this Section, the Company may recover attorneys' fees and reimbursement of its costs, provided
            that the Company has notified you in writing of the improperly filed claim, and you fail to promptly
            withdraw such claim.
          </li>
          <li>In the event that we make any material change to this arbitration provision (other than a change to our
            Arbitration Notice Address), you may reject any such change by sending us written notice to our Arbitration
            Notice Address within thirty (30) days of the change, in which case your Account and your license to use the
            Service will terminate immediately, and this Section, as in effect immediately prior to the amendments you
            reject, will survive the termination of these Terms.
          </li>
          <li>If only sub-section 15.9 paragraph above or the entirety of this Section 15 is found to be unenforceable,
            then the entirety of this Section 15 will be null and void and, in such case, the parties agree that the
            exclusive jurisdiction and venue described in Section 16 will govern any action arising out of or related to
            this Agreement.
          </li>
          <li>YOU UNDERSTAND THAT YOU WOULD HAVE HAD A RIGHT TO LITIGATE THROUGH A COURT, TO HAVE A JUDGE OR JURY DECIDE
            YOUR CASE, AND TO BE PARTY TO A className OR REPRESENTATIVE ACTION. HOWEVER, YOU UNDERSTAND AND AGREE TO
            HAVE ANY CLAIMS DECIDED INDIVIDUALLY AND ONLY THROUGH BINDING, FINAL, AND CONFIDENTIAL ARBITRATION.
          </li>
          <li>YOU HAVE THE RIGHT TO OPT-OUT OF THIS ARBITRATION PROVISION WITHIN THIRTY (30) DAYS FROM THE DATE THAT YOU
            FIRST USE, OR ATTEMPT TO USE, THE SERVICE BY WRITING TO support@liftstory.com OR TO THE ARBITRATION NOTICE
            ADDRESS. FOR YOUR OPT-OUT TO BE EFFECTIVE, YOU MUST SUBMIT A SIGNED WRITTEN NOTICE OPTING OUT AND CONTAINING
            ENOUGH DETAILS ABOUT YOU FOR US TO BE ABLE TO IDENTIFY YOU WITHIN THIRTY (30) DAYS. IF MORE THAN THIRTY (30)
            DAYS HAVE PASSED, YOU ARE NOT ELIGIBLE TO OPT OUT OF THIS PROVISION AND YOU MUST PURSUE YOUR CLAIM THROUGH
            BINDING ARBITRATION AS SET FORTH IN THIS AGREEMENT.
          </li>
        </ol>

        <h4>GOVERNING LAW</h4>

        <ol className="info-text">
          <li>The laws of England and Wales, excluding its conflicts of law principles, govern these Terms and your use
            of the Service.
          </li>
          <li>
            To the extent that any action relating to any dispute hereunder is permitted to be brought in a court of
            law, such action will be subject to the exclusive jurisdiction of:
            <ol>
              <li>the state and federal courts in the City of Alexandria, Virginia – if you are a resident of the United
                States; or
              </li>
              <li>the courts of the British Virgin Islands – if you are not a resident of the United States;and you
                hereby irrevocably submit to personal jurisdiction and venue in such courts, and waive any defense of
                improper venue or inconvenient forum.
              </li>
            </ol>
          </li>
        </ol>

        <h4>Miscellaneous Provisions</h4>

        <ol className="info-text">
          <li>No delay or omission by us in exercising any of our rights occurring upon any noncompliance or default by
            you with respect to these Terms will impair any such right or be construed to be a waiver thereof, and a
            waiver by the Company of any of the covenants, conditions or agreements to be performed by you will not be
            construed to be a waiver of any succeeding breach thereof or of any other covenant, condition or agreement
            hereof contained.
          </li>
          <li>Subject to Section 15, if any provision of these Terms is found to be invalid or unenforceable, then these
            Terms will remain in full force and effect and will be reformed to be valid and enforceable while reflecting
            the intent of the parties to the greatest extent permitted by law.
          </li>
          <li>Except as otherwise expressly provided herein, these Terms set forth the entire agreement between you and
            the Company regarding its subject matter, and supersede all prior promises, agreements or representations,
            whether written or oral, regarding such subject matter.
          </li>
          <li>The Company may transfer or assign any and all of its rights and obligations under these Terms to any
            other person, by any way, including by novation, and by accepting these Terms you give the Company consent
            to any such assignment and transfer. You confirm that placing on the Service of a version of these Terms
            indicating another person as a party to the Terms shall constitute valid notice to you of the transfer of
            Company's rights and obligations under the Agreement (unless otherwise is expressly indicated).
          </li>
          <li>All information communicated on the Service is considered an electronic communication. When you
            communicate with us through or on the Service or via other forms of electronic media, such as e-mail, you
            are communicating with us electronically. You agree that we may communicate electronically with you and that
            such communications, as well as notices, disclosures, agreements, and other communications that we provide
            to you electronically, are equivalent to communications in writing and shall have the same force and effect
            as if they were in writing and signed by the party sending the communication. You further acknowledge and
            agree that by clicking on a button labeled "SUBMIT", "CONTINUE", "REGISTER", "I AGREE" or similar links or
            buttons, you are submitting a legally binding electronic signature and are entering into a legally binding
            contract. You acknowledge that your electronic submissions constitute your agreement and intent to be bound
            by these Terms. YOU HEREBY AGREE TO THE USE OF ELECTRONIC SIGNATURES, CONTRACTS, ORDERS AND OTHER RECORDS
            AND TO ELECTRONIC DELIVERY OF NOTICES, POLICIES AND RECORDS OF TRANSACTIONS INITIATED OR COMPLETED THROUGH
            THE SERVICE.
          </li>
          <li>In no event shall the Company be liable for any failure to comply with these Terms to the extent that such
            failure arises from factors outside the Company's reasonable control.
          </li>
        </ol>

        <h4>Contact Details</h4>

        <p className="info-text">
          Funplaceapp Limited
          Vavylonos, 3, Latsia, 2237, Nicosia, Cyprus
          Contact email: support@liftstory.com
        </p>

      </div>
    </section>
  );
};

export default Terms;
