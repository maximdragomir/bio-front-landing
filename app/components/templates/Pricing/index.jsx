import React from 'react';

import PricingPlan from "./components/PricingPlan";
import {useAos} from "../../../hooks";
import {plansData} from "../../../data/plans";

import styles from "./Pricing.module.scss";

const Pricing = ({}) => {
  useAos();

  return (
    <section className={`section ${styles['pricing']}`}>
      <div className={`container container_responsive`}>
        <h1
          className={`title ${styles['pricing__title']}`}
          data-aos={'fade-up'}
          data-aos-delay={'100'}
        >
          Pricing</h1>
        <p
          className={`subtitle ${styles['pricing__subtitle']}`}
          data-aos={'fade-up'}
          data-aos-delay={'300'}
        >
          Here are our pricing plans. Clear and transparent.</p>

        <div className={styles['pricing__cards']} data-aos={'fade-up'} data-aos-delay={'500'}>
          {plansData.map((item) => <PricingPlan key={item.name} data={item} />)}
        </div>
      </div>
    </section>
  );
};

export default Pricing;
