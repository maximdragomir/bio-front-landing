import {Icon} from "../../../../elements/Icon";

import styles from "./PricingPlan.module.scss";
import {linkGenerator} from "../../../../../services/linkGenerator";
import {amplitudeEvent} from "../../../../../libs/amplitude";

const PricingPlan = ({data}) => {
  const {name, sum, type, list, action: {text, modifier, eventName, context}} = data;

  const modifierClass = styles[`plan_${name.toLowerCase()}`];

  const planAction = () => {
    amplitudeEvent(eventName, {context});
    linkGenerator()
  }

  return (
    <div className={`${styles['plan']} ${modifierClass}`}>
      <div className={styles['plan__header']}>
        <span className={styles['plan__name']}>{name}</span>
        <div className={styles['plan__price']}>
          <span className={styles['plan__price-sum']}>{sum}</span>
          <span className={styles['plan__price-type']}>/ {type}</span>
        </div>
      </div>
      <div className={styles['plan__body']}>
        <ul className={styles['plan__list']}>
          {list.map((item)=>{
            const {active, text} = item;
            return (
              <li key={text} className={`${styles['plan__item']} ${active ? styles['plan__item_active'] : ''}`}>
                <Icon name={active ? 'accept' : 'error'} className={`${styles['plan__item-icon']} ${active ? styles['plan__item-icon_yes'] : styles['plan__item-icon_no']}`} />
                <span className={styles['plan__item-text']}>{text}</span>
              </li>
            )
          })}
        </ul>
        <div className={styles['plan__action']}>
          <button
            className={`
              ${styles['c-button']}
              ${styles['plan__button']}
              ${modifier ? styles['c-button_' + modifier] : ''}
              ${modifier ? styles['plan__button_' + modifier] : ''}
            `}
            type='button'
            onClick={planAction}
          >
            {text}
          </button>
        </div>
      </div>
    </div>
  )
};

export default PricingPlan;
