import styles from './WhomCard.module.scss'
import {SITE_NAME} from "../../../../../utils/constants";

const WhomCard = ({data, id}) => {
	const {url, title, subtitle} = data
	return (
		<div className={`${styles['whomCard']} ${styles['whomCard_style-' + id]}`}>
			<div className={styles['whomCard__url']}>
				<span className={`${styles['whomCard__url-text']} ${styles['whomCard__url-text_default']}`}>{SITE_NAME}/</span>
				<span className={styles['whomCard__url-text']}>{url}</span>
			</div>
			<picture className={styles['whomCard__logo']}>
				<img src={`/images/section-whom/${url}.jpeg`} alt={title}/>
			</picture>
			<p className={styles['whomCard__title']}>{title}</p>
			<p className={styles['whomCard__subtitle']}>{subtitle}</p>
		</div>
	);
};

export default WhomCard;
