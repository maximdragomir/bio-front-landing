import styles from './BenefitsCard.module.scss'
import {Icon} from "../../../../elements/Icon";

const BenefitsCard = ({data, ...props}) => {
	const {title, items, id, visits = '', stats = ''} = data;

	const renderList = (type) => {
		switch (type) {
			case 'color':
				return (
					items.map((item, index)=>(
						<div key={index} style={{background: item}} className={styles['benefitsCard__item']} />
					))
				);
			case 'themes':
			case 'font':
				 return (
					 items.map((item, index)=>(
						 <picture key={item} className={styles['benefitsCard__item']}>
							 <img src={item} alt={title}/>
						 </picture>
					 ))
				 )
			case 'ceo':
				 return (
					 <>
						 <div className={`${styles['benefitsCard__item']} ${styles['benefitsCard__item_header']}`}>
							 <span className={`${styles['benefitsCard__item-name']}`}>Country</span>
							 <span className={`${styles['benefitsCard__item-name']}`}>Visits</span>
						 </div>
						 {items.map((item, index) => (
							 <div key={index} className={`${styles['benefitsCard__item']}`}>
								 <span className={`${styles['benefitsCard__item-wrapper']}`}>
									 <picture className={styles['benefitsCard__item-icon']}>
										 <img src={item.image} alt={title}/>
									 </picture>
									 <span className={`${styles['benefitsCard__item-name']}`}>{item.name}</span>
								 </span>
								 <span className={`${styles['benefitsCard__item-numbers']}`}>{item.numbers}</span>
							 </div>
							))}
					 </>
				 )
			case 'clicks':
				 return (
					 <>
						 <div className={`${styles['benefitsCard__item']} ${styles['benefitsCard__item_header']}`}>
							 <span className={`${styles['benefitsCard__item-name']} ${styles['benefitsCard__item-name_active']}`}>Links</span>
							 <span className={`${styles['benefitsCard__item-name']}`}>Buttons</span>
						 </div>
						 {items.map((item, index) => (
							 <div key={index} className={`${styles['benefitsCard__item']}`}>
								 <span className={`${styles['benefitsCard__item-wrapper']}`}>
									 <Icon name={item.name} className={`${styles['benefitsCard__item-icon']} ${styles['benefitsCard__item-icon_' + item.name]}`} />
									 <span className={`${styles['benefitsCard__item-name']}`}>{item.name}</span>
								 </span>
								 <span className={`${styles['benefitsCard__item-numbers']}`}>{item.numbers}</span>
							 </div>
						 ))}
					 </>
				 )
			case 'visits':
				 return (
					 <>
						 <span className={`${styles['benefitsCard__text']} ${styles['benefitsCard__text_size-xl']}`}>{visits}</span>
						 <span className={`${styles['benefitsCard__wrapper']}`}>
							 <span className={`${styles['benefitsCard__text']} ${styles['benefitsCard__text_green']}`}>{stats}</span>
							 {' '}
							 <span className={`${styles['benefitsCard__text']}`}>vs yesterday</span>
						 </span>
					 </>
				 )
		}
	}

	return (
		<div className={`${styles['benefitsCard']} ${styles['benefitsCard_' + id]}`} {...props}>
			<p className={`${styles['benefitsCard__title']}`}>{title}</p>
			<div className={`${styles['benefitsCard__list']}`}>
				{renderList(id)}
			</div>
		</div>
	);
};

export default BenefitsCard;
