import styles from './SocialCard.module.scss'
import {Icon} from "../../../../elements/Icon";
import {SITE_NAME} from "../../../../../utils/constants";

const SocialCard = ({iconName, logoSrc, id, ...props}) => {

	return (
		<div className={`${styles['socialCard']} ${styles['socialCard_style-' + id]}`} {...props}>
			<Icon name={iconName} className={`${styles['socialCard__icon']}`}/>
			<picture className={`${styles['socialCard__logo']}`}>
				<img src={logoSrc} alt=""/>
			</picture>
			<div className={`${styles['socialCard__decor']}`} />
			<p className={`${styles['socialCard__text']}`}>{SITE_NAME}/yoursite here</p>
			<div className={`${styles['socialCard__decor']} ${styles['socialCard__decor_bottom']}`} />
		</div>
	);
};

export default SocialCard;
