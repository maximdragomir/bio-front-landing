import styles from './Card.module.scss'

const Card = ({data, id = 1, ...props}) => {
	const {modifier, title, image} = data;

	return (
		<div className={`${styles['card']} ${modifier ? styles['card_size-' + modifier] : ''}`} {...props}>
				{/*<span style={{position: 'absolute',top: 0, color: 'red',}}>{id}</span>*/}
				<p className={styles['card__title']}>{title}</p>
				<picture className={styles['card__image']}>
					<img src={image} alt=""/>
				</picture>
		</div>
	);
};

export default Card;
