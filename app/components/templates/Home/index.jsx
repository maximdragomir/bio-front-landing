import React from 'react'
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {Autoplay} from 'swiper';
SwiperCore.use([Autoplay]);
import 'swiper/css';

import Card from "./components/Card";
import BioSite from "../../modules/BioSite";
import SocialCard from "./components/SocialCard";
import Tabs from "../../elements/Tabs";
import BenefitsCard from "./components/BenefitsCard";
import WhomCard from "./components/WhomCard";
import Banner from "../../modules/Banner";

import {
  heroBioSitesData,
  heroCardsData,
  socialCardsData,
  exampleBioSitesData,
  benefitsCardsData,
  benefitsCardsData2,
  whomCardsData,
} from "../../../data";
import {HERO_SWIPER_CONFIG, SMOOTH_SWIPER_CONFIG} from "../../../utils/config";
import {useWindowSize, useAos, useDebounce, useMounted} from "../../../hooks";
import {linkGenerator} from "../../../services/linkGenerator";
import {amplitudeEvent} from "../../../libs/amplitude";
import * as fbq from "../../../libs/fpixel";

import styles from './Home.module.scss';
import {MainContext} from "../../../context/MainContext";
import {CSSTransition} from "react-transition-group";


const Home = () => {
  // Context
  const {loader} = React.useContext(MainContext);

	// Data
	const [swiper, setSwiper] = React.useState(null);
	const [swiper2, setSwiper2] = React.useState(null);

  // Actions
  const heroAction = () => {
    amplitudeEvent('landing start button pressed', {place: 'main'});
    linkGenerator();
  }
  const exampleAction = () => {
    amplitudeEvent('landing start button pressed', {place: 'button 4'});
    linkGenerator();
  }

	// Hooks
	useAos();
  const loaded = useMounted(true, () => {
    amplitudeEvent('landing shown')
  });
	const debounceAutoplay = useDebounce(() => {
		swiper?.autoplay?.start();
		swiper2?.autoplay?.start();
	}, 1000);
	const windowSize = useWindowSize();
	React.useEffect(() => {
		debounceAutoplay();

		return () => {
			swiper?.autoplay?.stop();
			swiper2?.autoplay?.stop();
		}
	}, [windowSize])
	React.useEffect(() => {
		if(loaded) {
			debounceAutoplay();
		} else {
			swiper?.autoplay?.stop();
			swiper2?.autoplay?.stop();
		}
	}, [loaded])

	return (
		<>
			<section className={`section ${styles['hero']}`}>
				<div className={`container`}>
          <div className={styles['hero__wrapper']} id={'show-hero'}>
            <div
              className={styles['hero__info']}
              data-aos={'fade-up'}
              data-aos-delay={'100'}
              data-aos-anchor={'#show-hero'}
            >
              <h1 className={styles['hero__title']} onClick={()=>fbq.event('SignUp')}>One site with all your links</h1>
              <p className={styles['hero__subtitle']}>All your social profiles, websites and other content in one fully customizable and shareable link</p>
              <button className={`${styles['c-button']} ${styles['hero__button']}`} onClick={heroAction} type='button'>Start for FREE</button>
            </div>
            <CSSTransition in={!loader} timeout={250} classNames='transition-fade' unmountOnExit >
              <div
                className={styles['hero__bio-wrapper']}
                // data-aos={'fade-up'}
                // data-aos-delay={'100'}
                // data-aos-anchor={'#show-hero'}
              >
                <Swiper
                  onSwiper={setSwiper}
                  className={`${styles['hero__bio-slider']}`}
                  slidesPerView={HERO_SWIPER_CONFIG.SLIDE_PER_VIEW}
                  spaceBetween={HERO_SWIPER_CONFIG.SLIDE_BETWEEN}
                  loop={true}
                  initialSlide={0}
                  simulateTouch={false}
                  allowTouchMove={false}
                  noSwiping={false}
                  resizeObserver={false}
                  speed={HERO_SWIPER_CONFIG.SPEED}
                  autoplay={{
                    disableOnInteraction: HERO_SWIPER_CONFIG.DIS_INTERACTION,
                    delay: HERO_SWIPER_CONFIG.DELAY,
                    reverseDirection: HERO_SWIPER_CONFIG.REVERSE,
                  }}
                  breakpoints={{
                    1001: {
                      initialSlide: HERO_SWIPER_CONFIG.INITIAL_SLIDE
                    },
                  }}
                >
                  {heroBioSitesData.map((item, index)=>(
                    <SwiperSlide key={item.title} className={`${styles['hero__bio-slide']}`}>
                      <BioSite data={item} name={index + 1} className={`${styles['hero__bio-site']}`} />
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </CSSTransition>
          </div>
          <CSSTransition in={!loader} timeout={250} classNames='transition-fade' unmountOnExit >
          <div
            className={styles['hero__cards-wrapper']}
            // data-aos={'fade-up'}
            // data-aos-delay={'400'}
            // data-aos-anchor={'#show-hero'}
          >
            <Swiper
              onSwiper={setSwiper2}
              className={`${styles['hero__cards-slider']}`}
              slidesPerView={HERO_SWIPER_CONFIG.SMALL_SLIDE_PER_VIEW}
              spaceBetween={HERO_SWIPER_CONFIG.SMALL_SLIDE_BETWEEN}
              loop={true}
              resizeObserver={false}
              simulateTouch={false}
              allowTouchMove={false}
              noSwiping={false}
              centeredSlides={true}
              speed={HERO_SWIPER_CONFIG.SPEED}
              autoplay={{
                disableOnInteraction: HERO_SWIPER_CONFIG.DIS_INTERACTION,
                delay: HERO_SWIPER_CONFIG.DELAY,
                reverseDirection: HERO_SWIPER_CONFIG.REVERSE,
              }}
              breakpoints={{
                1001: {
                  centeredSlides: false
                },
              }}
            >
              {heroCardsData.map((item, index)=>(
                <SwiperSlide key={item.title} className={`${styles['hero__cards-slide']}`}>
                  <Card data={item} id={index + 1}/>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
          </CSSTransition>
				</div>
			</section>
			<div className={`container container_responsive`}>
        <Banner type={'default'} />
			</div>
			<section className={`section ${styles['social']}`}>
				<div className={`container`}>
					<div id={'show-social'} className={`row ${styles['social__block']}`}>
						<div
							className={`${styles['social__info']}`}
							// data-aos={'fade-right'}
							// data-aos-delay={'100'}
							// data-aos-anchor={'#show-social'}
						>
							<h2 className={`${styles['social__title']}`}>Add one link to all socials</h2>
							<p className={`${styles['social__description']}`}>Add your bio site link to all your social pages bio and help people to find more information about you</p>
						</div>
						<div className={`${styles['social__image']}`}>
							<SocialCard
								id={1}
								iconName='instagram'
								logoSrc='/images/section-social/logo1.jpeg'
								data-aos={'fade-right'}
								data-aos-delay={'300'}
								data-aos-anchor={'#show-social'}
							/>
							<SocialCard
								id={2}
								iconName='tiktok'
								logoSrc='/images/section-social/logo2.jpeg'
								data-aos={'fade-up-left'}
								data-aos-delay={'300'}
								data-aos-anchor={'#show-social'}
							/>
							<SocialCard
								id={3}
								iconName='snapchat'
								logoSrc='/images/section-social/logo2.jpeg'
								data-aos={'fade-down-right'}
								data-aos-delay={'300'}
								data-aos-anchor={'#show-social'}
							/>
							<SocialCard
								id={4}
								iconName='behance'
								logoSrc='/images/section-social/logo2.jpeg'
								data-aos={'fade-left'}
								data-aos-delay={'300'}
								data-aos-anchor={'#show-social'}
							/>
						</div>
					</div>
					<div className={`row ${styles['social__cards']}`}>
						{socialCardsData.map((item, index)=>(
							<Card
								key={index}
								data={item}
								id={index}
								data-aos={'fade-up'}
								data-aos-delay={index * 100}
							/>
						))}
					</div>
				</div>
			</section>
			<section className={`section ${styles['example']}`}>
				<div className={'container'}>
					<h2
						className={styles['example__title']}
						// data-aos={'fade-up'}
						// data-aos-delay={'100'}
					>
						Get started with Lift Bio
					</h2>
					<Tabs
						id={'show-tabs'}
						classes={{
							tabs: styles['tabs'],
							header: styles['tabs__header'],
							button: styles['tabs__button'],
							activeButton: styles['tabs__button_active'],
							content: styles['tabs__content'],
						}}
						defaultActive={'icons'}
						// data-aos={'fade-up'}
						// data-aos-delay={'100'}
						// data-aos-anchor={'#show-tabs'}
					>
						<Tabs.Item dataKey={'icons'} title={'Add socials'}>
							<div className={`${styles['tabs__wrapper']}`}>
								<picture className={styles['tabs__image']}>
									{/* RETINA */}
									<source
										type={'image/webp'}
										srcSet={'/images/section-example/slide1/bg-x2.webp'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									<source
										srcSet={'/images/section-example/slide1/bg-x2.jpeg'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									{/* RETINA */}
									<source srcSet={'/images/section-example/slide1/bg.webp'} type={'image/webp'} />
									<img src={'/images/section-example/slide1/bg.jpeg'} alt="slide1"/>
								</picture>
								<BioSite data={exampleBioSitesData[0]} name={['tab', 'tab1']} animate={'socials'} className={styles['tabs__bio-site']} />
							</div>
						</Tabs.Item>
						<Tabs.Item dataKey={'buttons'} title={'Add buttons'}>
							<div className={`${styles['tabs__wrapper']}`}>
								<picture className={styles['tabs__image']}>
									{/* RETINA */}
									<source
										type={'image/webp'}
										srcSet={'/images/section-example/slide2/bg-x2.webp'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									<source
										srcSet={'/images/section-example/slide2/bg-x2.jpeg'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									{/* RETINA */}
									<source srcSet={'/images/section-example/slide2/bg.webp'} type={'image/webp'} />
									<img src={'/images/section-example/slide2/bg.jpeg'} alt="slide2"/>
								</picture>
								<BioSite data={exampleBioSitesData[1]} name={['tab', 'tab2']} animate={'button'} className={styles['tabs__bio-site']} />
							</div>
						</Tabs.Item>
						<Tabs.Item dataKey={'theme'} title={'Choose theme'}>
							<div className={`${styles['tabs__wrapper']}`}>
								<picture className={styles['tabs__image']}>
									{/* RETINA */}
									<source
										type={'image/webp'}
										srcSet={'/images/section-example/slide3/bg-x2.webp'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									<source
										srcSet={'/images/section-example/slide3/bg-x2.jpeg'}
										media={'(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 144dpi)'}
									/>
									{/* RETINA */}
									<source srcSet={'/images/section-example/slide3/bg.webp'} type={'image/webp'} />
									<img src={'/images/section-example/slide3/bg.jpeg'} alt="slide3"/>
								</picture>
								<BioSite data={exampleBioSitesData[2]} name={['tab', 'tab3']} animate={'bg'} className={styles['tabs__bio-site']} />
							</div>
						</Tabs.Item>
					</Tabs>
					<div className={styles['example__action']} data-aos={'fade-up'} data-aos-delay={'100'} data-aos-anchor={'#show-tabs'}>
						<button className={`${styles['c-button']} ${styles['example__button']}`} onClick={exampleAction} type='button'>Start for FREE</button>
					</div>
				</div>
			</section>
			<section className={`section ${styles['benefits']}`}>
				<div className={'container'}>
					<div id={'show-benefits'} className={`row ${styles['benefits__block']}`}>
						<div className={`${styles['benefits__image']} ${styles['benefits__image_left']}`}>
							{benefitsCardsData.map((item, index)=>(
								<BenefitsCard key={index} data={item} data-aos={item.aos} data-aos-delay={'100'} data-aos-anchor={'#show-benefits'} />
							))}
						</div>
						<div
              className={`${styles['benefits__info']}`}
              // data-aos={'fade-left'}
              // data-aos-delay={'100'}
              // data-aos-anchor={'#show-benefits'}
            >
							<h2 className={`${styles['benefits__title']}`}>Customizable design</h2>
							<p className={`${styles['benefits__description']}`}>Make your Liftbio site cool. Show your brand or personality through colors customization, fonts and images.</p>
							<ul className={`${styles['benefits__list']}`}>
								<li className={`${styles['benefits__item']}`}>Pre-defined design themes</li>
								<li className={`${styles['benefits__item']}`}>Buttons, links and font styles</li>
								<li className={`${styles['benefits__item']}`}>Custom backgrounds</li>
								<li className={`${styles['benefits__item']}`}>Remove Lift Watermark</li>
							</ul>
						</div>
					</div>
					<div
            id={'show-benefits2'}
            className={`row ${styles['benefits__block']}`}
            // data-aos={'fade-right'}
            // data-aos-delay={'100'}
            // data-aos-anchor={'#show-benefits2'}
          >
						<div className={`${styles['benefits__info']} ${styles['benefits__info_left']} `}>
							<h2 className={`${styles['benefits__title']}`}>Track site activity</h2>
							<p className={`${styles['benefits__description']}`}>Keep track of how many times your liftbio site gets visited, how many linkclicks and buttonclicks  you have</p>
							<ul className={`${styles['benefits__list']}`}>
								<li className={`${styles['benefits__item']}`}>Individual link/button analytics</li>
								<li className={`${styles['benefits__item']}`}>Total links and buttons clicks, CTR</li>
								<li className={`${styles['benefits__item']}`}>Clicks and views geography</li>
								<li className={`${styles['benefits__item']}`}>Data export</li>
							</ul>
						</div>
						<div className={`${styles['benefits__image']} ${styles['benefits__image_right']}`}>
							{benefitsCardsData2.map((item, index)=>(
								<BenefitsCard key={index} data={item} data-aos={item.aos} data-aos-delay={'100'} data-aos-anchor={'#show-benefits2'} />
							))}
						</div>
					</div>
				</div>
			</section>
			<section className={`section ${styles['whom']}`}>
				<div className={'container'}>
					<h2
            className={styles['whom__title']}
            // data-aos={'fade-up'}
            // data-aos-delay={'100'}
          >For whom</h2>
					<p
            className={styles['whom__subtitle']}
            // data-aos={'fade-up'}
            // data-aos-delay={'100'}
          >We can all have a variety of activities and interests, Liftbio can be useful to any business regardless of industry</p>

					<div
            className={`${styles['whom__block']}`}
            data-aos={'fade-up'}
            data-aos-delay={'100'}
          >
						<Swiper
							className={styles['whom__slider']}
							slidesPerView={'auto'}
							spaceBetween={12}
							loop={true}
							simulateTouch={false}
							allowTouchMove={false}
							noSwiping={false}
							speed={SMOOTH_SWIPER_CONFIG.SPEED}
							centeredSlides
							autoplay={{
								disableOnInteraction: SMOOTH_SWIPER_CONFIG.DIS_INTERACTION,
								delay: SMOOTH_SWIPER_CONFIG.DELAY,
								reverseDirection: SMOOTH_SWIPER_CONFIG.REVERSE,
							}}
							breakpoints={{
								769: {
									slidesPerView: 3,
									spaceBetween: 24,
								},
								1801: {
									slidesPerView: SMOOTH_SWIPER_CONFIG.SLIDE_PER_VIEW,
									spaceBetween: HERO_SWIPER_CONFIG.SMALL_SLIDE_BETWEEN
								},
							}}
						 >
							{whomCardsData.map((item, index)=>(
								<SwiperSlide className={styles['whom__slide']} key={index}>
                  <WhomCard id={index+1} data={item}	/>
                </SwiperSlide>
							))}
						</Swiper>
					</div>
				</div>
			</section>
			<div className={`container container_responsive`}>
        <Banner type={'bottom'} />
			</div>
		</>
	);
};

export default Home;
