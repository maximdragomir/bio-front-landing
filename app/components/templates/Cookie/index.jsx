import React from 'react';
import {useAos} from "../../../hooks";
import {MainContext} from "../../../context/MainContext";
import styles from "./Cookie.module.scss";

const Cookie = ({}) => {
  const {setCookies, getCookiesData} = React.useContext(MainContext);

  const cookieAction = () => {
    if(getCookiesData) {
      setCookies((state)=>({
        ...state,
        show: true,
        settings: true,
      }))
    } else {
      setCookies((state)=>({
        ...state,
        settings: true,
      }))
    }
  }

  useAos();

  return (
    <section className={`section cookie ${styles['cookie']}`} data-aos={'fade-up'} data-aos-delay={'100'}>
      <div className={`container container_text`}>
        <h1 className={'title'}>Cookie policy</h1>
        <p className="subtitle">Last updated: 12 November 2021</p>

        <p>
          This cookie policy explains how we use cookies and other similar technologies when you use our service, what
          tracking technologies are and why we use them. It also explains how you can control their use.
        </p>
        <p className="c1">
          <span>If you have any questions you can contact us at </span><span className="c18"><a className="c31"
                                                                                                href="mailto:support@liftstory.com">support@liftstory.com</a></span><span
          className="c5">.</span>
        </p>
        <h4>Cookies</h4>
        <p className="c1">
            <span className="c5">
                A cookie is a small text file which is placed onto your device (e.g. computer, smartphone or other electronic device) when you visit our website to store a range of information, for example, your language preference, or
                browser and device you are using to view the website. Those cookies are set by us and called first-party cookies. We also use third party cookies, which are cookies from a domain different from the domain of our website, for
                our advertising and marketing efforts.
            </span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1"><span className="c5">Session cookies expire each time you close your browser and do not remain on your device afterwards. These cookies allow our website to link your actions during a particular browser session.</span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1">
            <span className="c5">
                Persistent cookies will remain on your device for a period of time, and will expire on a set expiration date, or when you delete them manually from your cache. These cookies are stored on your device in between browser
                sessions and allow your preferences and actions across our website to be remembered.
            </span>
        </p>
        <h4>Similar technologies</h4>
        <p className="c1"><span className="c5">In addition to cookies, there are other similar technologies that may be used on the web or in mobile ecosystems.</span>
        </p>
        <ul className="c33 lst-kix_x0mypn31r6vk-0 start">
          <li className="c1 c7 li-bullet-0">
            <em>Web beacon</em>
            <span className="c5">
                    &nbsp;is a small, transparent image (also known as &ldquo;pixel tag&rdquo;, &ldquo;web bug&rdquo;, &ldquo;GIF&rdquo;) that contains a unique identifier. It is embedded into a website or an email. When your browser reads
                    the code of the website it communicates with our server in order to display the image and, through this process, we are able to acquire information such as technical specifications of your device, operating systems and
                    settings being used. It can also track your activities during a session. Also, it enables us to recognize when each email was opened, from what IP address and from what device. We use this information to improve our
                    email communication.
                </span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_x0mypn31r6vk-0">
          <li className="c1 c7 li-bullet-0">
            <em>Software development kits (SDK)</em>
            <span className="c5">
                    &nbsp;are third-party software development kits that may be installed in our mobile applications. SDKs help us understand how you interact with our mobile applications and collect certain information about the device and
                    network you use to access the application.
                </span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_x0mypn31r6vk-0">
          <li className="c1 c7 li-bullet-0">
            <em>Local shared objects</em>
            <span>
                    , commonly called &ldquo;Flash cookies&rdquo;, are pieces of data that websites which use Adobe Flash may store on a user&#39;s computer to facilitate the Flash functionality. We may use Adobe Flash to display graphics,
                    interactive animations and other enhanced functionality. Local shared objects can track similar parameters to cookies but they can also provide information on your use of the specific feature that the cookie is enabling.
                    For example, if it facilitates the display of a video then we can receive details of how much of the video was watched and when viewing stopped. Flash cookie management tools can be accessed directly via
                </span>
            <span className="c18"><a className="c31"
                                     href="https://www.google.com/url?q=http://www.adobe.com&amp;sa=D&amp;source=editors&amp;ust=1636737819372000&amp;usg=AOvVaw2v6aZCkwu6LNL8lWZt3iE-">www.adobe.com</a></span><span>.</span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_x0mypn31r6vk-0">
          <li className="c1 c7 li-bullet-0">
            <em>HTML5 local storage</em>
            <span className="c5">
                    . HTML 5 is the fifth version of the HTML language and contains functionality that allows information to be stored locally within the browser&rsquo;s data files. HTML5 local storage operates in a similar way to cookies
                    but differs in that it can hold greater amounts of information and does not rely on an exchange of data with the website&rsquo;s server.
                </span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_x0mypn31r6vk-0">
          <li className="c1 c7 li-bullet-0">
            <em>Fingerprinting</em>
            <span className="c5">
                    &nbsp;is a technique that involves combining a set of information elements in order to uniquely identify a particular device. These information elements include, for example: data from configuration of the device, CSS
                    information, JavaScript objects, installed fonts, installed plugins with the browser, use of any APIs, HTTP header information, clock information.
                </span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_l7wgmg9fmbq2-0 start">
          <li className="c1 c7 li-bullet-0">
            <em>Device Identifiers</em>
            <span className="c5">
                    &nbsp;are identifiers comprised of numbers and letters, which are unique to each specific device. These include Apple&rsquo;s ID for Advertisers (IDFA) and Google&rsquo;s Android Advertising ID (AAID). They are stored on
                    the device and are used to recognize you and your devices across different apps and devices for marketing and advertising purposes. You can reset your device identifier or opt out of personalized advertising in the
                    settings of your device.
                </span>
          </li>
        </ul>
        <p className="c1 c9 9"><span className="c5"/></p>
        <ul className="c33 lst-kix_u8tsveho4i-0 start">
          <li className="c1 c7 li-bullet-0">
            <em>Social widgets</em>
            <span className="c5">
                    &nbsp;are buttons or icons provided by third-party social media providers that allow you to interact with those social media services when you view a web page or a mobile app screen. These social widgets may collect
                    browsing data, which may be received by the third party that provided the widget, and are controlled by the third parties.
                </span>
          </li>
        </ul>
        <h4>How can you manage your cookies and similar technologies?</h4>
        <ol className="c33 lst-kix_rxsh2cncngyi-0 start">
          <li className="c1 c7 li-bullet-0"><strong className="3 c30">Consent management tool</strong></li>
        </ol>
        <p className="c1">
          <span>You can manage your cookie preferences on a consent management platform that appears when you visit the website or go to the </span><span
          className="btn_show_cookie" onClick={cookieAction}>consent management platform</span> from this policy.
          <span className="c5">
                It may be necessary to refresh the page for the updated settings to take effect. Essential cookies cannot be disabled, nor can the tool be used to block cookies on third-party websites linked to our website.
            </span>
        </p>
        <ol className="c33 lst-kix_rxsh2cncngyi-0">
          <li className="c1 c7 li-bullet-0"><strong className="3 c30">Browser and device settings</strong></li>
        </ol>
        <p className="c1">
            <span className="c5">
                Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for doing so vary from browser to browser, and from version to version. You can however obtain up-to-date information about blocking and
                deleting cookies via these links:
            </span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <ul className="c33 lst-kix_wnubzgxmoz7-0 start">
          <li className="c1 c7 li-bullet-0">
            <span className="c18"><a className="c31"
                                     href="https://www.google.com/url?q=https://support.google.com/chrome/answer/95647&amp;sa=D&amp;source=editors&amp;ust=1636737819374000&amp;usg=AOvVaw2N-pRnjMhiTluuu0UYhhgN">Chrome</a></span>
          </li>
          <li className="c1 c7 li-bullet-0">
                <span className="c18">
                    <a
                      className="c31"
                      href="https://www.google.com/url?q=https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences&amp;sa=D&amp;source=editors&amp;ust=1636737819375000&amp;usg=AOvVaw2PN8aXmA70E1ZXxhs7CHRS"
                    >
                        Firefox
                    </a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
                <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=https://www.opera.com/help/tutorials/security/cookies/&amp;sa=D&amp;source=editors&amp;ust=1636737819375000&amp;usg=AOvVaw2x5g7oubVUKbOjH2hrpZGt">Opera</a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
                <span className="c18">
                    <a
                      className="c31"
                      href="https://www.google.com/url?q=https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies&amp;sa=D&amp;source=editors&amp;ust=1636737819375000&amp;usg=AOvVaw136KytvjI39Zvx6C8mmbaf"
                    >
                        Internet Explorer
                    </a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
                <span className="c18">
                    <a
                      className="c31"
                      href="https://www.google.com/url?q=https://support.apple.com/guide/safari/manage-cookies-and-website-data-sfri11471/mac&amp;sa=D&amp;source=editors&amp;ust=1636737819376000&amp;usg=AOvVaw0Sdv2kSoNeRKD-X_sOaEfG"
                    >
                        Safari
                    </a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
                <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy&amp;sa=D&amp;source=editors&amp;ust=1636737819376000&amp;usg=AOvVaw3PO1TSrKAXN8-ZDNbVU0">
                        Edge
                    </a>
                </span>
          </li>
        </ul>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1">
          <span>Detailed instructions on how to control your cookies through browser settings can also be found here: </span>
          <span className="c18">
                <a className="c31"
                   href="https://www.google.com/url?q=https://www.aboutcookies.org/how-to-control-cookies/&amp;sa=D&amp;source=editors&amp;ust=1636737819377000&amp;usg=AOvVaw0YSfGWJZOBl22Ca2Vyb3mc">
                    https://www.aboutcookies.org/how-to-control-cookies/
                </a>
            </span>
          <span className="c5">.</span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1"><span className="c5">Please note that blocking all cookies will have a negative impact on the usability of many websites. If you block cookies, you will not be able to use all the features on our website.</span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1">
          <span>To reset your device identifier or opt-out of personalized advertising, follow </span>
          <span className="c18">
                <a className="c31"
                   href="https://www.google.com/url?q=https://support.google.com/googleplay/android-developer/answer/6048248?hl%3Den&amp;sa=D&amp;source=editors&amp;ust=1636737819377000&amp;usg=AOvVaw110gLxJ-ibsip4EntyhP8W">
                    Google instructions
                </a>
            </span>
          <span>&nbsp;or </span>
          <span className="c18">
                <a
                  className="c31"
                  href="https://www.google.com/url?q=https://support.apple.com/en-gb/HT205223%23:~:text%3DTo%2520reset%2520your%2520Advertising%2520Identifier%2520on%2520macOS%252C%2520choose%2520Apple%2520menu,be%2520less%2520relevant%2520to%2520you.&amp;sa=D&amp;source=editors&amp;ust=1636737819378000&amp;usg=AOvVaw3nkrCMkEDbi-m-Rud4JN9S"
                >
                    Apple instructions
                </a>
            </span>
          <span className="c5">.</span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <p className="c1">
          <span>To opt out of data usage by Google Analytics follow instructions: </span>
          <span className="c18">
                <a className="c31"
                   href="https://www.google.com/url?q=https://tools.google.com/dlpage/gaoptout&amp;sa=D&amp;source=editors&amp;ust=1636737819378000&amp;usg=AOvVaw2zfFHYx9Xn0XGIaEz0SZxO">
                    https://tools.google.com/dlpage/gaoptout
                </a>
            </span>
          <span className="c5">.</span>
        </p>
        <ol className="c33 lst-kix_rxsh2cncngyi-0" start="3">
          <li className="c1 c7 li-bullet-0"><strong className="3 c30">Opt-out of internet-based advertising</strong>
          </li>
        </ol>
        <p className="c1">
            <span className="c5">
                The third-party advertisers, ad agencies and vendors with which we work may be members of the Network Advertising Initiative, the Digital Advertising Alliance Self-Regulatory Program for Online Behavioral Advertising, the
                European Digital Advertising Alliance. To opt-out of interest-based advertising from the participating companies, visit the following links:
            </span>
        </p>
        <p className="c1 c9"><span className="c5"/></p>
        <ul className="c33 lst-kix_z7is0oklf4af-0 start">
          <li className="c1 c7 li-bullet-0">
            <span>Network Advertising Initiative </span>
            <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=http://optout.networkadvertising.org/&amp;sa=D&amp;source=editors&amp;ust=1636737819379000&amp;usg=AOvVaw33X68fvQO67hOxiRKY5XMm">http://optout.networkadvertising.org/</a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
            <span>Digital Advertising Alliance </span>
            <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=http://optout.aboutads.info/&amp;sa=D&amp;source=editors&amp;ust=1636737819379000&amp;usg=AOvVaw0MA8EnOczAGTRdfoxz5ajB">http://optout.aboutads.info/</a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
            <span>Digital Advertising Alliance (Canada) </span>
            <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=http://youradchoices.ca/choices&amp;sa=D&amp;source=editors&amp;ust=1636737819380000&amp;usg=AOvVaw0xOT5Qe2cv1AsPCkNZnI93">http://youradchoices.ca/choices</a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
            <span>Digital Advertising Alliance (EU) </span>
            <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=http://www.youronlinechoices.com/&amp;sa=D&amp;source=editors&amp;ust=1636737819380000&amp;usg=AOvVaw3bFRAHVWVd07yQFWrEw-2d">http://www.youronlinechoices.com/</a>
                </span>
          </li>
          <li className="c1 c7 li-bullet-0">
            <span>DAA AppChoices page </span>
            <span className="c18">
                    <a className="c31"
                       href="https://www.google.com/url?q=http://www.aboutads.info/appchoices&amp;sa=D&amp;source=editors&amp;ust=1636737819380000&amp;usg=AOvVaw3mnLVYf-LXUUtnekt1j9hB">http://www.aboutads.info/appchoices</a>
                </span>
          </li>
        </ul>
        <h4>Cookies list</h4>
        <p className="c1"><span className="c5">You can find more information about the cookies and similar technologies we use and the purposes for which we use them in the table below:</span>
        </p>
        <ol className="c33 lst-kix_gg0fibvbgt4u-0 start" start="1">
          <li className="c1 c7 li-bullet-0">
            <strong className="">Strictly necessary cookies</strong>
            <span>
                    . These cookies are necessary for the website to function and cannot be switched off in our systems. They are usually only set in response to actions made by you which amount to a request for services, such as setting
                    your privacy preferences, logging in or filling in forms. You can set your browser to block or alert you about these cookies, but some parts of the website will not then work.
                </span>
            <span className="c5">Strictly necessary cookies enable you to benefit from our website features. </span>
          </li>
        </ol>
        <div style={{overflow: 'scroll'}}>

          <table className="" style={{border: '1px'}}>
            <tbody>
            <tr className="c15">
              <td className="2" colSpan="1" rowSpan="1">
                <strong>Cookie</strong>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <strong>Type</strong>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <strong>Source</strong>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <strong>Purpose</strong>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <strong>Expiry</strong>
              </td>
            </tr>
            <tr className="c59">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">temporary-token</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="5"><span className="c0">This cookie is used to register your payment in our system.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c53">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">name</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className=""><span className="">This c</span><span className="c0">ookie helps store user&rsquo;s name value.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c37">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">lift_user_id</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className=""><span
                  className="">This cookie is used for registering your payment in our system.</span></p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">plan</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className=""><span className="">This cookie helps store information about the subscription plan you have chosen.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">__paypal_storage__</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="c6" colSpan="1" rowSpan="1">
                <p className="c1"><span className="c52">This cookie is </span><span className="c52">used for processing your transaction.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">state</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="c6" colSpan="1" rowSpan="1">
                <p className="c1"><span className="">This cookie helps store y</span><span className="">our last visited page on our website.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="">email</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className=""><span className="">This cookie helps store your email for sending you a confirmation email.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">question</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="2">
                <p className=""><span
                  className="c0">These cookies help define the user&#39;s path on our website.</span></p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">variant</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">unsubscribe-email</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store your email for cancelling the subscription.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">auth.token</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store the token for sending cancel subscription request.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">visit-discount</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps determine the applied discount to your subscription.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">functional</span></p>
                <p className=" c9"><span className="c0" /></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store the user&#39;s functional cookie consent state for the current domain.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">analytics</span></p>
                <p className=" c9"><span className="c0" /></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store the user&#39;s analytics cookie consent state for the current domain.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">marketing</span></p>
                <p className=" c9"><span className="c0" /></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store the user&#39;s marketing cookie consent state for the current domain.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="2" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">cookie</span></p>
              </td>
              <td className="8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c39" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="0" colSpan="1" rowSpan="1">
                <p className="c13"><span className="c0">This cookie helps store the user&#39;s cookie consent state for the current domain.</span>
                </p>
              </td>
              <td className="c3" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
        <p className="c1 c9 9"><span className="c5" /></p>
        <p className="c1 c9 9"><span className="c5" /></p>
        <ol className="c33 lst-kix_gg0fibvbgt4u-0" start="2">
          <li className="c1 c7 li-bullet-0">
            <strong className="">Analytical or performance cookies</strong>
            <span className="c5">
                    . These allow us to recognise and count the number of visitors, to know which pages are the most and least popular, and to see how visitors move around our website when they are using it. This helps us to improve the way
                    our website works, for example, by ensuring that users are finding what they are looking for easily. If you do not allow these cookies we will not know when you have visited our website, and will not be able to monitor
                    its performance.
                </span>
          </li>
        </ol>
        <div style={{overflow: "scroll"}}>

          <table className="">
            <tbody>
            <tr className="c15">
              <td className=" " colSpan="1" rowSpan="1">
                <strong>Cookie</strong>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <strong>Type</strong>
              </td>
              <td className=" " colSpan="1" rowSpan="1">
                <strong>Source</strong>
              </td>
              <td className="1 " colSpan="1" rowSpan="1" style={{minWidth: '200px'}}>
                <strong>Purpose</strong>
              </td>
              <td className="c8 " colSpan="1" rowSpan="1">
                <strong>Expiry</strong>
              </td>
            </tr>
            <tr className="c54">
              <td className=" " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">_ga</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className=" " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Google Analytics</span></p>
              </td>
              <td className=" 1" colSpan="1" rowSpan="4">
                <p className="5">
                            <span className="">
                                This cookie enables us to distinguish unique users, remember the number and time of their previous visits, remember traffic source information, determine the start and end of sessions. We use Google Analytics
                                to analyse the use of our website.
                            </span>
                  <span className="c18">
                                <a className="c31"
                                   href="https://www.google.com/url?q=https://policies.google.com/privacy&amp;sa=D&amp;source=editors&amp;ust=1636737819423000&amp;usg=AOvVaw3nOlDelV-ZR_A1ZnHy-6">Privacy Policy</a>
                            </span>
                </p>
              </td>
              <td className="c8 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days</span></p>
              </td>
            </tr>
            <tr className="c54">
              <td className=" " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">_gid</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className="c1"><span className="">Google Analytics</span></p>
              </td>
              <td className="c8 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1 day</span></p>
              </td>
            </tr>
            <tr className="c54">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">_gat</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className="c1"><span className="">Google Analytics</span></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1 day</span></p>
              </td>
            </tr>
            <tr className="c54">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="">GCLID_STORAGE_KEY</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className="c1"><span className="c0">Google Analytics</span></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="">userAdData</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="1" colSpan="1" rowSpan="1">
                <p className="5"><span
                  className="c0">We use these cookies to store from where you came to our website.</span></p>
                <p className="5 c9"><span className="c0" /></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="">amplitude-user-id</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="5" colSpan="1" rowSpan="1">
                <p className="1"><span className="c0">This cookie helps store the Amplitude user identifier for analytics purposes.</span>
                </p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">amplitude_unsent_identify_*</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Amplitude</span></p>
              </td>
              <td className="5" colSpan="1" rowSpan="3">
                <p className="1">
                  <span
                    className="">These cookies are used by Amplitude for session tracking for analytics purposes. </span>
                  <span className="c18">
                                <a className="c31"
                                   href="https://www.google.com/url?q=https://amplitude.com/privacy&amp;sa=D&amp;source=editors&amp;ust=1636737819436000&amp;usg=AOvVaw3Tcfj4mrUPbLmvhoOfWq_n">Amplitude Privacy Policy.</a>
                            </span>
                </p>
                <p className="1"><span className="c0">&nbsp;</span></p>
                <p className="c9 1"><span className="c0" /></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">amplitude_unsent_*</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Amplitude</span></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="c10">
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">amp_bcff4d</span></p>
              </td>
              <td className="7" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Amplitude</span></p>
              </td>
              <td className="c8" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">365 days</span></p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
        <ol className="c33 lst-kix_gg0fibvbgt4u-0" start="3">
          <li className="c1 c7 li-bullet-0">
            <strong className=" ">Functionality cookies</strong>
            <span className="c0">
                    .These are used to recognise you when you return to our website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region). They
                    may be set by us or by third party providers whose services we have added to our pages. If you do not allow these cookies then some or all of these services may not function properly.
                </span>
          </li>
        </ol>
        <div style={{overflow: 'scroll'}}>

          <table className="">
            <tbody>
            <tr className="c15">
              <td className="c50 " colSpan="1" rowSpan="1">
                <strong>Cookie</strong>
              </td>
              <td className="c55 " colSpan="1" rowSpan="1">
                <strong>Type</strong>
              </td>
              <td className=" c51" colSpan="1" rowSpan="1">
                <strong>Source</strong>
              </td>
              <td className="3 " colSpan="1" rowSpan="1" style={{minWidth: '350px'}}>
                <strong>Purpose</strong>
              </td>
              <td className=" 7" colSpan="1" rowSpan="1">
                <strong>Expiry</strong>
              </td>
            </tr>
            <tr className="c37">
              <td className=" c50" colSpan="1" rowSpan="1">
                <p className=""><span className="">TRANSACTION_ID</span></p>
              </td>
              <td className=" c55" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c51 " colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className=" 3" colSpan="1" rowSpan="1">
                <p className="5"><span className="c0">This cookie helps store user&rsquo;s transaction identifier</span>
                </p>
              </td>
              <td className="7 " colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
        <p className="c1 c9"><span className="c0" /></p>
        <ol className="c33 lst-kix_gg0fibvbgt4u-0" start="4">
          <li className="c1 c7 li-bullet-0">
            <strong>Marketing cookies</strong>
            <span className="c0">
                    . These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your
                    interests. These cookies may be set through our website by our advertising partners. They may be used by those companies to build a profile of your interests and show you relevant adverts on other sites.
                </span>
          </li>
        </ol>

        <div style={{overflow: 'scroll'}}>
          <table className="">
            <tbody>
            <tr className="c61">
              <td className="0 " colSpan="1" rowSpan="1">
                <strong>Cookie</strong>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <strong>Type</strong>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <strong>Source</strong>
              </td>
              <td className="c19" colSpan="1" rowSpan="1" style={{minWidth: '200px'}}>
                <strong>Purpose</strong>
              </td>
              <td className=" c36" colSpan="1" rowSpan="1">
                <strong>Expiry</strong>
              </td>
            </tr>
            <tr className="c56">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">_fbp</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className=" c32" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Facebook</span></p>
              </td>
              <td className="c19" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">This cookie is used to deliver a series of advertisement products such as real-time bidding from third party advertisers using Facebook.</span>
                </p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="">18</span><span className="c0">0 days</span></p>
              </td>
            </tr>
            <tr className="c57">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">fr</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">3rd party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Facebook</span></p>
              </td>
              <td className="c19" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">This cookie enables ad delivery or retargeting. It contains a browser and user unique ID combination, used for targeted advertising.</span>
                </p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">90 days</span></p>
              </td>
            </tr>
            <tr className="8">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="">FB_PAGE_VIEW_EVENT_SENT_STORAGE_KEY</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="">Lift</span></p>
              </td>
              <td className="c19" colSpan="1" rowSpan="6">
                <p className="5"><span className="c0">These cookies help define the user&#39;s path on our website and send corresponding events to Facebook.</span>
                </p>
                <p className="5 c9"><span className="c0" /></p>
                <p className="5 c9"><span className="c0" /></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="">730 days </span></p>
              </td>
            </tr>
            <tr className="8">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">FB_ADD_TO_CART_EVENT_SENT_STORAGE_KEY</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="8">
              <td className=" 0" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">FB_INITIATE_CHECKOUT_EVENT_SENT_STORAGE_KEY</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="8">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">ALREADY_SCROLLED_TO_BUT_STORAGE_KEY</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="8">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">RESULT_PAGE_OPENED_STORAGE_KEY</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            <tr className="8">
              <td className="0 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">payment-success</span></p>
              </td>
              <td className="c11" colSpan="1" rowSpan="1">
                <p className=""><span className="c0">1st party</span></p>
              </td>
              <td className="c32 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">Lift</span></p>
              </td>
              <td className="c36 " colSpan="1" rowSpan="1">
                <p className=""><span className="c0">730 days </span></p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
        <p className="c1"><strong>Changes to this policy</strong></p>
        <p className="c1"><span className="c0">We may change this policy from time to time, when we do we will inform you by updating the &ldquo;Last updated&rdquo; date below.</span>
        </p>

      </div>
    </section>
  );
};

export default Cookie;
