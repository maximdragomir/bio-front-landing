import React from 'react';

import Banner from "../../modules/Banner";
import {useAos} from "../../../hooks";

import styles from './Contacts.module.scss'

const Contacts = () => {
  useAos();

  return (
    <section className={`section ${styles['contacts']}`}>
      <div className={`container`}>
        <h1 className={`title ${styles['contacts__title']}`} data-aos={'fade-up'} data-aos-delay={'100'}>Contact us</h1>
        <div className={styles['contacts__info']}>
          <div className={`${styles['contacts__column']}`} data-aos={'fade-up'} data-aos-delay={'100'}>
            <div className={styles['contacts__subtitle']}>
              <span className={styles['contacts__text']}>support</span>
            </div>
            <p className={styles['contacts__description']}>
              <span className={styles['contacts__text']}>If you can’t find the answer you’re looking for, please send an email to</span>
              {' '}
              <a href='mailto: support@liftstory.com' className={`${styles['contacts__text']} ${styles['contacts__text_link']}`}>support@liftstory.com</a>
            </p>
          </div>
          <div className={styles['contacts__column']} data-aos={'fade-up'} data-aos-delay={'100'}>
            <div className={styles['contacts__subtitle']}>
              <span className={styles['contacts__text']}>lift bio’s office</span>
            </div>
            <p className={styles['contacts__description']}>
              <span className={styles['contacts__text']}>Vavylonos, 3, Latsia, 2237, Nicosia, Cyprus</span>
            </p>
          </div>
        </div>
        <div id='show-map' className={styles['contacts__decor']}>
          <picture className={styles['contacts__map']}>
            <img src={'/images/map/map.png'} alt="map" data-aos={'fade-up'} data-aos-delay={'300'} data-aos-anchor={'#show-map'}/>
          </picture>
          <picture className={styles['contacts__pin']}>
            <img src={'/images/map/pin.svg'} alt="pin" data-aos={'fade-down'} data-aos-delay={'800'} data-aos-duration={'250'} data-aos-anchor={'#show-map'}/>
          </picture>
        </div>
      </div>
      <div className={`container container_responsive`}>
        <Banner type={'bottom'} />
      </div>
    </section>
  );
};

export default Contacts;
