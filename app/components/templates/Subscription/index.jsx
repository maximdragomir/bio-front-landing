import {useAos} from "../../../hooks/useAos";

import styles from './Subscription.module.scss';

const Subscription = ({}) => {
  useAos();

  return (
    <section className={`section terms ${styles['subscription']}`} data-aos={'fade-up'} data-aos-delay={'100'}>
      <div className={`container container_text`}>
        <h1 className={'title'}>Subscription Terms</h1>
        <p className="subtitle">Last Updated: November, 2020</p>

        <p className="info-text">Unless you cancel at least 24 hours before the end of the free trial, you will be
          automatically charged a price indicated on the payment screen or/and Apple’s payment pop-up screen for a
          chosen subscription period.</p>

        <p className="info-text">The subscription renews automatically at the end of each period (each week, month, 6
          months, year, or otherwise) until you cancel.</p>

        <p className="info-text">Payment will be charged to your iTunes & App Store/Apple ID account ("Account") at
          confirmation of purchase (after you accept by Touch ID, Face ID, or otherwise the subscription terms on the
          pop-up screen provided by Apple) or after the end of the free trial.</p>

        <p className="info-text">You can cancel a free trial or subscription anytime by turning off auto-renewal through
          your <a href="https://apps.apple.com/account/subscriptions">Account settings</a>. </p>

        <p className="info-text">To avoid being charged, cancel the subscription in your <a
          href="https://apps.apple.com/account/subscriptions">Account settings</a> at least 24 hours before the end of
          the free trial or the current subscription period.</p>

        <p className="info-text">You alone can manage your subscriptions. Learn more about managing subscriptions (and how
          to cancel them) on <a href="https://support.apple.com/en-ca/HT202039">Apple support page</a>. Note that
          deleting the app does not cancel your subscriptions (however, if you use Apple’s IOs 13 or later IOs versions,
          Apple provides you with a convenient way to manage subscriptions when you delete an app).</p>

        <p className="info-text">If you purchased a subscription through the Apple App Store and are eligible for a refund,
          you'll have to request it directly from Apple. To request a refund, follow these instructions from the <a
            href="https://support.apple.com/en-us/HT204084">Apple support page</a>.</p>

        <p className="info-text">Please make a screenshot of this information for your reference. This may help you to
          control your subscriptions.</p>
      </div>
    </section>
  )
}

export default Subscription;
