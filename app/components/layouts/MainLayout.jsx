import React from "react";
import {CSSTransition, SwitchTransition} from "react-transition-group";

import Head from "next/head";
import ComponentAnimation from "../elements/ComponentAnimation";
import Loader from "../elements/Loader";
import Header from "../modules/Header";
import Footer from "../modules/Footer";
import MyCookies from "../modules/MyCookies";
import {useGetCookies, useDebounce} from "../../hooks";
import {cookieAccess} from "../../helpers";
import {MainContext} from "../../context/MainContext";
import {LOADER_CONFIG} from "../../utils/config";

export function MainLayout({children, title = 'Lift', id = ''}) {
  // loading imitation
  const [loader, setLoader] = React.useState(true);
  const [cookies, setCookies] = React.useState({
    show: false,
    settings: false,
  });

  const getCookiesData = useGetCookies('cookiesData');
  const getCookiesSettings = useGetCookies('cookiesSettings');

  const debounceLoader = useDebounce(() => {
    setLoader(false);
  }, LOADER_CONFIG.DELAY);
  React.useEffect(() => {
    !loader && setCookies((state)=>({
      ...state,
      show: cookieAccess() && !getCookiesData
    }))
  }, [loader])
  React.useEffect(() => {
    debounceLoader();

    return () => {
      setLoader(false)
    }
  }, [])

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="UTF-8"/>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
        <meta httpEquiv="X-UA-Compatible" content="ie=edge"/>
        <meta name="theme-color" content="#ffffff"/>
        <meta name="description" content="Lift"/>
      </Head>
      <div className='app'>
        {/*<SwitchTransition>*/}
        {/*  <CSSTransition*/}
        {/*    key={id === 'index' && loader}*/}
        {/*    addEndListener={(node, done) => node.addEventListener("transitionend", done, false)}*/}
        {/*    classNames={LOADER_CONFIG.ANIMATION}*/}
        {/*  >*/}
        {/*    {id === 'index' && loader ?*/}
        {/*      <Loader />*/}
        {/*     : (*/}
              <div className='wrapper'>
                <MainContext.Provider value={{setCookies, getCookiesData, loader}}>
                  <Header/>
                  <main className='main'>
                    {children}
                  </main>
                  <Footer />
                </MainContext.Provider>
              </div>
        {/*    )}*/}
        {/*  </CSSTransition>*/}
        {/*</SwitchTransition>*/}
        <ComponentAnimation state={cookies.show} animationName={getCookiesData ? 'transition-fade' : 'transition-fadeMixBottom'}>
          <MyCookies cookies={cookies} setCookies={setCookies} getCookies={{getCookiesData, getCookiesSettings}} />
        </ComponentAnimation>
      </div>
    </>
  )
}
