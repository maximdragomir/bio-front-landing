import Head from "next/head";

export function EmptyLayout({children, title = 'Lift'}) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="UTF-8"/>
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
        <meta httpEquiv="X-UA-Compatible" content="ie=edge"/>
        <meta name="theme-color" content="#000000"/>
        <meta name="description" content="Lift"/>
      </Head>
      <div className='app'>
        <main className='main'>
          {children}
        </main>
      </div>
    </>
  )
}
