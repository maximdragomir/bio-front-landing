import Link from "next/link";
import { useRouter } from "next/router";

const RouteLink = ({children, link, classes, activeClass}) => {
  const router = useRouter();

  return (
    <Link href={link}><a className={`${classes} ${activeClass && router.pathname === link ? activeClass : ''}`}>{children}</a></Link>
  );
};

export default RouteLink;
