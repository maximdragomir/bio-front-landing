import React from 'react';
import {CSSTransition} from "react-transition-group";

const ComponentAnimation = ({children, state, animationName, timeout = 250}) => {
  return (
    <CSSTransition
      in={state}
      timeout={timeout}
      classNames={animationName}
      unmountOnExit
    >
      {children}
    </CSSTransition>
  );
};

export default ComponentAnimation;
