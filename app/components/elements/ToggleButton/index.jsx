import React from "react";

import styles from './ToggleButton.module.scss'

const ToggleButton = ({className, checked, updateToggleValue}) => {
  const [localChecked, setLocalChecked] = React.useState(checked);

  const toggleAction = (event) => {
    setLocalChecked(event.target.checked)
    updateToggleValue(event.target.checked)
  }

  React.useEffect(() => {
    setLocalChecked(checked)
  }, [checked])

  return (
    <label
      className={`
        ${styles['toggle']}
        ${className}
      `}
    >
      <input
        type="checkbox"
        className={styles['toggle__checkbox']}
        checked={localChecked}
        onChange={toggleAction}
      />
      <span className={styles['toggle__slider']}/>
    </label>
  );
};

export default ToggleButton;
