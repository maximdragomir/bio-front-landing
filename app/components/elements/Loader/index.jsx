import {CSSTransition} from "react-transition-group";
import {useMounted} from "../../../hooks";

import styles from './Loader.module.scss'

const Loader = ({}) => {
  const componentMounted = useMounted(true); // show loader

  return (
    <CSSTransition in={componentMounted} timeout={500} classNames='transition-fadeTop'>
      <div className={styles['loader']}>
          <div className={styles['loader__logo']}>
              <img src={'/images/logo.svg'} alt="Lift logo"/>
          </div>
      </div>
    </CSSTransition>
  );
};

export default Loader;
