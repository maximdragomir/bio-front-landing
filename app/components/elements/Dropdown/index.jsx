import React from 'react';
import { Transition } from 'react-transition-group';


const Index = ({state, header, content}) => {
  const defaultStyle = {
    height: 0,
    willChange: 'height',
    transition: 'height 0.25s ease 0.1s', // 0.25 + 0.1 = 0.35 Transition timeout
  };
  const [updateHeight, setUpdateHeight] = React.useState({ ...defaultStyle });
  const updatingStyles = {
    entering: { ...updateHeight },
    entered: { ...updateHeight },
    exiting: { ...defaultStyle },
    exited: { ...defaultStyle },
  };
  const refContent = React.useRef();
  React.useEffect(() => {
    const contentBounding = refContent?.current?.childNodes[0].getBoundingClientRect();

    setUpdateHeight({
      ...defaultStyle,
      height: contentBounding?.height,
    });
  }, [state]);

  return (
    <>
      {header}
      <Transition in={state} timeout={350} unmountOnExit>
        {(state) => (
          <div
            ref={refContent}
            style={{
              ...defaultStyle,
              ...updatingStyles[state],
              overflow: 'hidden',
            }}
          >
            {content}
          </div>
        )}
      </Transition>
    </>
  )
};

export default Index;
