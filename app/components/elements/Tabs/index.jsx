import React from 'react';
import {normalizedArray} from '../../../utils';

const Tabs = ({classes, defaultActive, ...props}) => {
	// Data
	const [activeTab, setActiveTab] = React.useState(defaultActive);

	// Filtered active tab
	const child = normalizedArray(props.children)
		.filter((child) => child.props.dataKey === activeTab)
		.shift();

	// Renders
	const renderTabsButton = (
		normalizedArray(props.children).map((child, index) => (
			<button
				key={index}
				className={`
					${child.props.dataKey === activeTab ? classes.activeButton : ''}
					${classes.button || ''}
			 	`}
				onClick={() => setActiveTab(child.props.dataKey)}
			>
				{child.props.title}
			</button>
		))
	)
	const renderContent = React.cloneElement(child, {...child.props});

	return (
		<div className={classes.tabs || ''} {...props}>
			<div className={classes.header || ''}>
				{renderTabsButton}
			</div>
			<div className={`${classes.content || ''}`}>
				{renderContent}
			</div>
		</div>
	);
};

Tabs.Item = (props) => {
	return normalizedArray(props.children);
};

export default Tabs;
