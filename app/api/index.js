import axios from "axios";
import {API_DOMAIN} from "../utils/constants";

export const sendFBData = (data) => {
  return new Promise((resolve, reject) => {
    return axios
      .post(API_DOMAIN.FB, data)
      .then((response) => {
        resolve(response);
      })
      .catch(reject);
  })
}
