import {MainLayout} from "../app/components/layouts/MainLayout";
import Privacy from "../app/components/templates/Privacy";

export default function privacy() {
  return (
    <MainLayout title={'Privacy policy'}>
      <Privacy />
    </MainLayout>
  )
}
