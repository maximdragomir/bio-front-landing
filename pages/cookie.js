import {MainLayout} from "../app/components/layouts/MainLayout";
import Cookie from "../app/components/templates/Cookie";

export default function cookie() {
  return (
    <MainLayout title={'Cookie policy'}>
      <Cookie />
    </MainLayout>
  )
}
