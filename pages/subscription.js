import {MainLayout} from "../app/components/layouts/MainLayout";
import Subscription from "../app/components/templates/Subscription";

export default function subscription() {
  return (
    <MainLayout title={'Subscription policy'}>
      <Subscription />
    </MainLayout>
  )
}
