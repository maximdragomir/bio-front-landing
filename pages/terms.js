import {MainLayout} from "../app/components/layouts/MainLayout";
import Terms from "../app/components/templates/Terms";

export default function terms() {
  return (
    <MainLayout title={'Terms and Service'}>
      <Terms />
    </MainLayout>
  )
}
