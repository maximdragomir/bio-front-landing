import {MainLayout} from "../app/components/layouts/MainLayout";
import Home from "../app/components/templates/Home";

export default function index({}) {
  return (
    <MainLayout title={'Lift'} id={'index'}>
      <Home />
    </MainLayout>
  )
}
