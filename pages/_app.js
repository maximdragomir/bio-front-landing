import React from "react";
import Script from 'next/script'
import { useRouter } from 'next/router'
import * as fbq from "../app/libs/fpixel";
import '../app/styles/global.scss'
import {FB_PIXEL_ID} from "../app/utils/constants";
import {useMounted, useSetUserDataInLocalStorage} from "../app/hooks";
import {initAmplitude} from "../app/libs/amplitude";


const App = ({ Component, pageProps }) => {
  const router = useRouter();
  useSetUserDataInLocalStorage();
  React.useEffect(() => {
    fbq.pageview()

    const handleRouteChange = () => {
      fbq.pageview()
    }

    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events]);
  useMounted(false, () => {
    initAmplitude()
  })

    return (
      <>
        {/* Global Site Code Pixel - Facebook Pixel */}
        <Script
          id={'pixel'}
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', ${FB_PIXEL_ID});
          `,
          }}
        />
        <Component {...pageProps} />
      </>
    )
}

export default App;


// function SafeHydrate({ children }) {
//     return (
//         <div suppressHydrationWarning>
//             {typeof window === 'undefined' ? null : children}
//         </div>
//     )
// }
// return <SafeHydrate><Component {...pageProps} /></SafeHydrate>
