import {MainLayout} from "../app/components/layouts/MainLayout";
import Contacts from "../app/components/templates/Contacts";

export default function contacts() {
  return (
    <MainLayout title={'Contacts'}>
      <Contacts/>
    </MainLayout>
  )
}
