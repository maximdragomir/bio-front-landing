import {MainLayout} from "../app/components/layouts/MainLayout";
import Pricing from "../app/components/templates/Pricing";

export default function pricing() {
  return (
    <MainLayout title={'Pricing'}>
      <Pricing/>
    </MainLayout>
  )
}
