import Document, {Html, Head, Main, NextScript} from 'next/document'


export default class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx)
		return {...initialProps}
	}

	render() {
		return (
			<Html>
				<Head>
					<link rel="icon" type="image/svg+xml" href="/favicon.svg"/>
					<link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet"/>
				</Head>
				<body>
          <Main/>
          <NextScript/>
				</body>
			</Html>
		)
	}
}
