import {MainLayout} from "../app/components/layouts/MainLayout";

const ErrorPage = () => {
    return (
        <MainLayout>
          <div className="container">
            <h1 style={{textAlign: 'center'}}>404 Page not Found</h1>
          </div>
        </MainLayout>
    )
}

export default ErrorPage;